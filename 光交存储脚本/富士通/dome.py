__author__ = 'l4537'
#coding:utf-8
import time,os,re,sys
def Re(res,total,id = "1"):
    #1.普通模式正则，2，多行模式正则
    if id == "1":
        data = re.findall(str(res),total)
    if id == "2":
        data = re.findall(str(res),total,re.M)
    return data
class Fst():
    def __init__(self,file_name,id = 1):
        self.file_name = file_name
        self.id = id
        try:
            a = open(self.file_name,"r")
            self.data = a.read()
        except Exception as e:
            print("文件打开失败")
            sys.exit()
    def Host_wwn(self):
        a = Re(r"\s+(\d+)\s(\S+)\s+(\w{16})\s+\d+\s+(\S+)",self.data)
        b = []
        host_zu = []
        for i in a:
            i = list(i)
            ii = Re(r"(.*)[_|~|#]\d+",i[1])
            host_zu.append(str(ii[0]))
            b.append(i + ii)
        host_zu = set(host_zu)
        host_total ={}
        for i in b:
            for ii in host_zu:
                if i[4] == ii:
                    host_total[ii] ={}
        for i in b:
            for ii in host_zu:
                if i[4] == ii:
                    host_total[ii][i[1]] = {}
        for i in b:
            for ii in host_zu:
                if i[4] == ii:
                    host_total[ii][i[1]]["host_id"] = i[0]
                    host_total[ii][i[1]]["host_wwn"] = i[2]
                    host_total[ii][i[1]]["host_type"] = i[3]
        return host_total
    def Host_port(self):
        a = Re(r"\s(CM#\d CA#\d Port#\d)\s+(\d+)\s+(.*?)\s+(\S+)",self.data)
        host_name = []
        port_host = {}
        for i in a:
            host_name.append(i[2])
        host_name = set(host_name)
        for i in a:
            for ii in host_name:
                if ii == i[2]:
                    port_host[ii] = {"port":[]}
        for i in a:
            for ii in host_name:
                if ii == i[2]:
                    zz = port_host[ii]["port"]
                    zz.append(i[0])
                    port_host[ii]["port"] = zz
        return port_host
    def Lun_raid(self):
        if self.id == 1:
            a = Re(r"\s+(\d+)\s(\S+)\s+(\S+)\s+\S+\s+-\s+(\S+)\s(\S+)\s+(\d+)",self.data)
        elif self.id == 2:
            a = Re(r"\s+(\d+)\s(\S+)\s+(\S+)\s+\S+\s+(\d+)\s+(\S+)\s+(\d+)",self.data)
        lun_size = {}
        for i in a:
            lun_size[i[1]] ={}
            lun_size[i[1]]["id"] = i[0]
            lun_size[i[1]]["state"] = i[2]
            lun_size[i[1]]["raid_id"] = i[3]
            lun_size[i[1]]["raid_name"] = i[4]
            lun_size[i[1]]["size"] = i[5]
        return lun_size
    def Lun_map(self):
        if self.id == 1:
            a = Re(r"\s+(\d+)\s(\S+)\s+\S+\s+(\S+)\n<Mapping>\n\s+LUN  Affinity/LUN Group    Port\n\s+No.  Name\n\s+\d+\s+\d+\s(\S+)",self.data)
        elif self.id == 2:
            a = Re(r"\s+(\d+)\s(\S+)\s+\S+\s+(\S+)\n<Mapping>\n.*\n.*\n.*\n\s+\d+\s+\d+\s+(\S+)",self.data)
        host = []
        lun_group ={}
        for i in a:
            host.append(i[3])
        host = set(host)
        for i in host:
            lun_group[i] ={}
        for i in a:
            for ii in host:
                if i[3] == ii:
                    lun_group[i[3]][i[1]] = i[2]
        return lun_group
    def raid_group(self):
        if self.id == 1:
            a = Re(r"\d+\s+(\S+)\s+RAID(\d+)\s+\S+\s+\S+\s+(\d+)",self.data)
        elif self.id == 2:
            a = Re(r"\d+\s+(\S+)\s+RAID(\d+)\s+\S+\s+\S+\s+(\d+)",self.data)
        return a
    def Host_group(self):
        a = Re(r"\s+(\d+)\s(\S+)\s+\d+\s.*?FC/FCoE",self.data)
        return a
    def Port_host_lun(self):
        #端口组，主机组，lun组
        if self.id == 1:
            a = Re(r"Volumes\n\s+\d+\s+(\S+)\s+\d+\s+(\S+)\s+\d+\s+(\S+)",self.data)
        elif self.id == 2:
            a = Re(r"Volumes\s*\n.*\n\s+\d+\s+(\S+)\s+\d+\s+(\S+)\s+\d+\s+(\S+)",self.data)
        return a
    def Total(self):
        host_zu = self.Host_group()
        host_wwn = self.Host_wwn()
        host_port = self.Host_port()
        lun_raid = self.Lun_raid()
        lun_map = self.Lun_map()
        port_host_lun = self.Port_host_lun()
        raid_group_1 = self.raid_group()
        #查看raid组所有的空间
        stor_total = 0
        for i in raid_group_1:
            stor_total += int(i[2])
        #查看使用啦多少空间
        lun_a = []
        for i in lun_map.values():
            lun_a.extend(list(i.keys()))
        lun_a = set(lun_a)
        lun_total = 0
        for i in lun_a:
            for k in lun_raid:
                if i == k:
                    lun_total += int(lun_raid[k]["size"])
        while True:
            print("1：查看raid组及LUN组。\n2：查看主机组，主机wwn，及lun，及lun大小，及lun所在raid。\n3：通过wwn搜素确认主机组LUN及LUN所在的raid组\n4:通过前端口确认每个前端口使用的主机组")
            num = int(input("请选择:"))
            if num == 1:
                raid_num = 0
                raid_name = []
                for i in lun_raid:
                    raid_name.append(lun_raid[i]["raid_name"])
                raid_name = set(raid_name)
                for i in raid_name:
                    raid_num += 1
                    print("\033[1;32;40mraid组名称：\033[1m",i)
                    print("lun号码".ljust(10),"lun名称".ljust(15),"lun大小(MB)")
                    num_lun_total = 0
                    for ii in lun_raid:
                        if i == lun_raid[ii]["raid_name"]:
                            print(lun_raid[ii]["id"].ljust(10),ii.ljust(20),lun_raid[ii]["size"])
                            for qq in lun_map:
                                for qqq in lun_map[qq]:
                                    if qqq == ii:
                                        num_lun_total += int(lun_raid[ii]["size"])
                    print("该raid组总共用啦%sMB的空间"%(num_lun_total))
                print("该存储总共划啦%s个raid组，raid组总共大小为"%(raid_num))
            elif num == 2:
                tatal_size = 0
                for i in host_zu:
                    print("\033[1;32;40m主机组名称:\033[1m",i[1])
                    print("   主机名称".ljust(13),"主机类型".ljust(8),"主机ID".ljust(13),"主机WWN")
                    port_messages=[]
                    for ii in host_wwn:
                        if i[1] == ii:
                            for iii in host_wwn[ii]:
                                for iiii in host_port:
                                    if iiii == iii:
                                        print("   ",iii.ljust(20),host_wwn[ii][iii]["host_type"].ljust(10),host_wwn[ii][iii]["host_id"].ljust(10),host_wwn[ii][iii]["host_wwn"])
                                        port_messages.append(host_port[iiii]["port"])
                    if len(port_messages) != 0:
                        print("   使用前端口名称:",port_messages[0])
                    print("   LUN信息如下：")
                    lun_size = 0
                    print("   LUN_ID".ljust(10),"LUN名称".ljust(20),"LUN-UUID".ljust(28),"LUN大小(MB)".ljust(17),"所在raid组".ljust(18),"状态")
                    for iiiii in port_host_lun:
                        if i[1] == iiiii[1]:
                            for k in lun_map:
                                if iiiii[2] == k:
                                    for kk in lun_map[k]:
                                        for zz in lun_raid:
                                            if kk == zz:
                                                print("   ",lun_raid[zz]["id"].rjust(3),kk,lun_map[k][kk].rjust(40),lun_raid[zz]["size"].rjust(10),lun_raid[zz]["raid_name"].rjust(20),lun_raid[zz]["state"].rjust(20))
                                                lun_size += int(lun_raid[zz]["size"])
                                                tatal_size +=int(lun_raid[zz]["size"])
                    print("   该主机组总共划啦",lun_size,"MB")
                print("该存储一共规划啦%s的空间，已用啦%sMB的空间"%(stor_total,lun_total))

            elif num == 3:
                print(3)
if __name__ == "__main__":
    a = Fst("4001-0924",1)
    a.Total()

