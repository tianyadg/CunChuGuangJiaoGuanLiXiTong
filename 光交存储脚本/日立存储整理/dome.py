__author__ = 'l4537'
#coding:utf-8
import time,os,re,sys,json
def Re(res,total,id = "1"):
    #1.普通模式正则，2，多行模式正则
    if id == "1":
        data = re.findall(str(res),total)
    if id == "2":
        data = re.findall(str(res),total,re.M)
    return data
class Rili():
    def __init__(self,file_name):
        self.file_name = file_name
        try:
            a = open(self.file_name,"r")
            self.data = a.read()
        except Exception as e:
            print("文件打开失败",e)
            sys.exit()
    def Host_wwn(self):
        data = Re(r"(\d\w),(\S+),\d+,,(\w{16}),,\d+,(\w+-\w+)",self.data)
        return data
    def Host_lun(self):
        data = Re(r"\d\w,(\S+),\d+,,\w+,(..:..:..)",self.data)
        return data
    def Lun_messages(self):
        data = Re(r"(..:..:..),,OPEN-V,([Basic|Dynamic Provisioning]+),\w+,(\d+),(\d+).\d+,(\d+),.*?,,,,.*?,(\w+)!*,,,",self.data)
        return data
    def Total(self):
        host_wwn = self.Host_wwn()
        host_lun = self.Host_lun()
        lun_mes = self.Lun_messages()
        hostgroup = {}
        hostgroup_name = set(list(i[1] for i in host_wwn))
        hsotgroup_lun = {}
        for i in hostgroup_name:
            hostgroup[i] = {}
            port_mess = []
            cro_port_mess = []
            for ii in host_wwn:
                if i == ii[1]:
                    hostgroup[i][ii[2]] = {}
            for iii in host_wwn:
                if i == iii[1]:
                    port_mess.append(iii[0])
                    cro_port_mess.append(iii[3])
                    hostgroup[i][iii[2]]["port"] = port_mess
                    hostgroup[i][iii[2]]["cro_port"] = cro_port_mess
        #print(hostgroup)
        for zz in hostgroup:
            hsotgroup_lun[zz] = {}
            for i in host_lun:
                if zz == i[0]:
                    hsotgroup_lun[zz][i[1]] = {}
        for i in hsotgroup_lun:
            for ii in hsotgroup_lun[i]:
                for iii in lun_mes:
                    if ii == iii[0]:
                        hsotgroup_lun[i][ii]["type"] = iii[1]
                        hsotgroup_lun[i][ii]["size_Cyl"] = iii[2]
                        hsotgroup_lun[i][ii]["size"] = iii[3]
                        hsotgroup_lun[i][ii]["size_Blocks"] = iii[4]
                        hsotgroup_lun[i][ii]["pool"] = iii[5]
        print(hsotgroup_lun)
        use_pool = 0
        for i in hsotgroup_lun:
            for ii in hsotgroup_lun[i]:
                use_pool += int(hsotgroup_lun[i][ii]["size"])
        while True:
            print("1:查看组及信息\n2：查看资源池使用状况")
            num = int(input("请选择:"))

            if num == 1:
                for i in hostgroup_name:
                    print("主机组名称为:%s"%(i))
                    print("主机wwn情况如下:")
                    cro_port_name = []
                    port_name =[]
                    for ii in hostgroup:
                        if i == ii:
                            for iii in hostgroup[ii]:
                                print(iii)
                                port_name.extend(hostgroup[ii][iii]["port"])
                                cro_port_name.extend(hostgroup[ii][iii]["cro_port"])
                    print("使用前端口名称为:",set(port_name),"使用的控制器端口为:",set(cro_port_name))
                    print("LUN使用状态如下:")
                    a = 0
                    for ii in hsotgroup_lun:
                        if i == ii:
                            for iii in hsotgroup_lun[ii]:
                                print(iii,"%sGB"%(int(hsotgroup_lun[ii][iii]["size"])/1024),hsotgroup_lun[ii][iii]["type"],hsotgroup_lun[ii][iii]["pool"])
                                a += int(hsotgroup_lun[ii][iii]["size"])
                    print("该主机组一共使用啦%sG的空间"%(a/1024))
                print("该存储总共划啦%sT的空间"%(use_pool/1024/1024))
            elif num == 2:
                pool_name = set(list(i[5] for i in lun_mes))
                for i in pool_name:
                    print(i)
                    pool_total = 0
                    pool_use = 0
                    if len(i) >=2 :
                        for ii in lun_mes:
                            if i == ii[5]:
                                if ii[1] == "Basic":
                                    pool_total += int(ii[3])
                                    print("LDEV为",ii[0],ii[3])
                                if ii[1] == "Dynamic Provisioning":
                                    pool_use += int(ii[3])
                                    print("map的LUN为",ii[0],ii[3])
                    print("该资源池总共有%sT的存储，目前已划拨啦%s的空间"%(pool_total/1024/1024,pool_use/1024/1024))
if __name__ == "__main__":
    a = Rili("672.csv")
    c = a.Total()