__author__ = 'l4537'
#coding:utf-8
import time,os,re,sys,json
def Re(res,total,id = "1"):
    #1.普通模式正则，2，多行模式正则
    if id == "1":
        data = re.findall(str(res),total)
    if id == "2":
        data = re.findall(str(res),total,re.M)
    return data
class Rili():
    def __init__(self,file_name):
        self.file_name = file_name
        try:
            a = open(self.file_name,"r")
            self.data = a.read()
        except Exception as e:
            print("文件打开失败",e)
            sys.exit()
    def Host_wwn(self):
        data = Re(r"(\d\w),(\S+),\d+,,(\w{16}),,\d+,(\w+-\w+)",self.data)
        return data
    def Host_lun(self):
        data = Re(r"\d\w,(\S+),\d+,,\w+,(..:..:..)",self.data)
        return data
    def Lun_messages(self):
        data = Re(r"(..:..:..),,OPEN-V,([Basic|Dynamic Provisioning]+),\w+,(\d+),(\d+).\d+,(\d+),.*?,,,,.*?,(\w+)!*,,,",self.data)
        return data
    def Total(self):
        host_group = self.Host_wwn()
        host_wwn = self.Host_lun()
        lun_mes = self.Lun_messages()
        host_group_dict = {}
        host_group_lun_dict = {}
        Pool_group = {}
        host_gro = set(list((i[1] for i in host_group)))
        for i in host_gro:
            host_group_dict[i] = {}
            for ii in host_group:
                if i == ii[1]:
                     host_group_dict[i][ii[2]] = {}
            for ii in host_group:
                if i == ii[1]:
                    host_group_dict[i][ii[2]]["port"] = ii[0]
                    host_group_dict[i][ii[2]]["port_crot"] = ii[3]
        print(host_group_dict)
        for i in host_gro:
            host_group_lun_dict[i] ={}
            for ii in host_wwn:
                if i == ii[0]:
                    host_group_lun_dict[i][ii[1]] = {}
                    for iii in lun_mes:
                        if ii[1] == iii[0]:
                            host_group_lun_dict[i][ii[1]]["mode"] = iii[1]
                            host_group_lun_dict[i][ii[1]]["size_Cyl"] = iii[2]
                            host_group_lun_dict[i][ii[1]]["szie"] = iii[3]
                            host_group_lun_dict[i][ii[1]]["size_Blocks"] = iii[4]
                            host_group_lun_dict[i][ii[1]]["pool"] = iii[5]
        pool_name = list(set(list(i[5] for i in lun_mes)))
        for i in pool_name:
            Pool_group[i] = {}
            for ii in lun_mes:
                if i == ii[5]:
                    Pool_group[i][ii[0]] ={}
            for iii in lun_mes:
                if i == iii[5]:
                    Pool_group[i][iii[0]]["mode"] = iii[1]
                    Pool_group[i][iii[0]]["size_Cyl"] = iii[2]
                    Pool_group[i][iii[0]]["szie"] = iii[3]
                    Pool_group[i][iii[0]]["size_Blocks"] = iii[4]
        data = [host_group_dict,host_group_lun_dict,Pool_group]
        return data

def Main():
    a = Rili("221.csv")
    c = a.Total()
    host_group_wwn = c[0]
    host_group_lun = c[1]
    pool_group = c[2]
    print(pool_group)
    print("1:显示主机组及LUN的信息\n2:显示资源池的信息")
    while True:
        try:
            num = int(input("请选择"))
        except Exception as e:
            print("输入错误，请重新输入",e)
        if num == 1:
            for i in host_group_lun:
                print("\033[1;32;40m主机组名称为：\033[1m",i)
                print("wwn信息如下:".rjust(20))
                print("wwn:".rjust(25))
                for k in host_group_wwn:
                    port_num = []
                    port_cro = []
                    if k == i:
                        for kk in host_group_wwn[k]:
                            port_num.append(host_group_wwn[k][kk]["port"])
                            port_cro.append(host_group_wwn[k][kk]["port_crot"])
                            print(kk.rjust(36))
                        print("前端口名称：".rjust(20),set(port_num))
                        print("前端口控制器：".rjust(20),set(port_cro))
                num1 = 0
                print("LUN信息如下:".rjust(20))
                print("LUN号码".rjust(15),"资源池".rjust(10),"大小(MB)".rjust(10),"模式".rjust(20))
                for ii in host_group_lun[i]:
                    print(ii.rjust(20),host_group_lun[i][ii]["pool"].rjust(10),host_group_lun[i][ii]["szie"].rjust(10),host_group_lun[i][ii]["mode"].rjust(30))
                    num1 += int(host_group_lun[i][ii]["szie"])
                print("该主机组总共用啦".rjust(20),num1/1024/1024,"TB")
        elif num == 2:
            pass
if __name__ == "__main__":
    Main()