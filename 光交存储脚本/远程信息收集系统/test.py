__author__ = 'l4537'
#coding:utf-8
import pexpect
def ssh_conn(ip,root,password,command,login_s,run_s):
    try:
        cmd = "ssh "+root +"@"+ip
        child = pexpect.spawn(cmd)
        ret = child.expect([pexpect.TIMEOUT,login_s],timeout=5)
        if ret == 0:
            print("连接错误,请核实IP是否正确")
            return None
        child.sendline(password)
        child.expect(run_s)
        for i in command:
            child.sendline(i)
            #child.expect(run_s)
            type(child.expect(run_s))
            print(bytes.decode(child.before))
        child.close()
    except Exception as e:
        print("连接失败,请确认账号或者密码")

ip = "192.168.20.60"
root = "root"
password = "root"
cmd = ["df -h","uptime"]
login_s = "password:"
run_s ="[#]"
ssh_conn(ip,root,password,cmd,login_s,run_s)
