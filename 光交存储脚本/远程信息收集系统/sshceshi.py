# !/usr/bin/env python
# coding=utf-8
import pexpect

def ssh_cmd(ip, passwd, cmd):
    ret = -1
    ssh = pexpect.spawn('ssh admin@%s "%s"' % (ip, cmd))
    i = ssh.expect(['Password:', 'continue connecting (yes/no)?'], timeout=5)
    if i == 0 :
        ssh.sendline(passwd)
    elif i == 1:
        ssh.sendline('yes\n')
        ssh.expect('password: ')
        ssh.sendline(passwd)
    ssh.sendline(cmd)
    r = ssh.read()
    print(bytes.decode(r))
    ret = 0
    return ret

ssh_cmd("10.180.169.202","Jmo@CId$","show int")

