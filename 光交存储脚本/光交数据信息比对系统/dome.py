__author__ = 'l4537'
#coding:utf-8
import re,json
import dome_san
switchshowdata1 ={}
hardware1 ={}
#光纤链路收集
def dataswitch(file_name):
    file = open(file_name,'r')
    data = file.read()
    file_1 = re.compile(r'(.*)\S{6}\s+\S+\s+\w{2,3}\s+(\w+)\s+FC(.*)')
    a = file_1.findall(data)
    for i in a:
        z = i[0].split(" ")
        while "" in z:
            z.remove("")
        y = i[2].split(" ")
        if len(z) > 2:
            switchshowdata1[z[0]] = {"slot":z[1],"port":z[2],"status":i[1]}
        elif len(z) == 2:
            switchshowdata1[z[0]] = {"slot":"","port":z[1],"status":i[1]}
        while "" in y:
            y.remove("")
        if len(y) >= 2:
            switchshowdata1[z[0]]["wwn"] = y[1]
        else:
            switchshowdata1[z[0]]["wwn"] = "none"
    file.close()
#软硬件信息收集
def hardware(file_name):
    file = open(file_name,'r')
    data = file.read()
    file_1 = re.compile(r'((Chassis Factory Serial Num:\s+)|(CHASSIS/WWN.*\nHeader Version:.*\nPow.*\nFa.*\nFactory Serial Num:\s+))(\w+)')
    SN = file_1.findall(data)
    file_2 = re.compile(r'switchName:\s+(\w+)')
    switchName = file_2.findall(data)
    file_3 = re.compile(r'switchState:\s+(\w+)')
    switchState = file_3.findall(data)
    file_4 = re.compile(r'zoning:\s+ON\s\((\w+)\)')
    zoning = file_4.findall(data)
    file_5 = re.compile(r'switchDomain:\s+(\w+)')
    switchDomain = file_5.findall(data)
    file_6 = re.compile(r'Power Supply #(\d)\sis\s(\w+)')
    power = file_6.findall(data)
    file_7 = re.compile(r'Fan\s(\d)\sis\s(\w+)')
    Fan = file_7.findall(data)
    file_8 = re.compile(r'\d+\s+(UNKNOWN|SW BLADE|CP BLADE|CORE BLADE)(\s+\d+\s+\w+|\s+\w+)')
    slotshow = file_8.findall(data)
    file_9 = re.compile(r'CP(\d)\s+FOS\s+(.*?)\s+(\w+)')
    firmwareshow = file_9.findall(data)
    hardware1["san_messages"] = {"firmwareshow":firmwareshow,"slotshow":slotshow,"Fan":Fan,"power":power,"switchDomain":switchDomain[0],"switchName":switchName[0],"switchState":switchState[0],"zoning":zoning[0]}
    file.close()
#别名信息收集
def Alias(file_name):
    file = open(file_name,'r')
    data = file.read()
    file_1 = re.compile(r'alias:\s(.*)\n\s+(..:..:..:..:..:..:..:..)')
    alias = file_1.findall(data)
#    print(alias)
    for k in switchshowdata1:
        for i in alias:
            if switchshowdata1[k]["wwn"] == i[1]:
                switchshowdata1[k]["alias"] = i[0]
    for zz in switchshowdata1:
        if "alias" in switchshowdata1[zz].keys():
           pass
        else:
            switchshowdata1[zz]["alias"] = "none"
#            print(zz,switchshowdata1[k]["alias"])

    file.close()
#收集zone信息
def Zone(file_name):
    file = open(file_name,'r')
    data = file.read()
    file_1 = re.compile(r'zone:\s+(.*)\s+((..:..:..:..:..:..:..:..\s+|\d+,\d+\s+)+)')
    zone = file_1.findall(data)
    file_5 = re.compile(r'switchDomain:\s+(\w+)')
    switchDomain = file_5.findall(data)
    kkk = []
    for i in zone:
        b = i[1].split(" ")
        while "" in b:
            b.remove("")
        for j in b:
            jj = j.strip()
            jjj = [i[0],jj]
            kkk.append(jjj)
#    print(kkk)
    yyy = []
    for bb in switchshowdata1:
#        print(switchshowdata1[bb]["wwn"])
        for bbb in kkk:
            if switchshowdata1[bb]["wwn"] == bbb[1]:
                yyy.append(bbb[0])
            if str(switchDomain[0] + "," + str(bb)) == bbb[1]:
                yyy.append(bbb[0])
        yyyy = list(set(yyy))
        switchshowdata1[bb]["zonename"] = yyyy
        yyy = []
    file.close()
#收集端口速率信息
def Port_sulv(file_name):
    file = open(file_name,'r')
    data = file.read()
    file_1 = re.compile(r'(\d+):\s+(\d.*?)\s+(\d.*?)\s+')
    port_rx_tx = file_1.findall(data)
#    print(port_rx_tx)
    if len(port_rx_tx) > 0:
        for i in port_rx_tx:
            for k in switchshowdata1:
                if k == i[0]:
                    switchshowdata1[k]["tx"] = i[1]
                    switchshowdata1[k]["rx"] = i[2]
    else:
        for k in switchshowdata1:
            switchshowdata1[k]["tx"] = "none"
            switchshowdata1[k]["rx"] = "none"
    for zz in switchshowdata1:
        if "tx" in switchshowdata1[zz].keys():
           pass
        else:
            switchshowdata1[zz]["tx"] = "none"
        if "rx" in switchshowdata1[zz].keys():
           pass
        else:
            switchshowdata1[zz]["rx"] = "none"
#启动信息
def Rightshow(file_name):
    file = open(file_name,'r')
    data = file.read()
    rightshow = re.findall(r"Slot\s+(\d{1,2})/Port\s+(\d+).*?RX Power:\s+(-\d+.\d+).*?TX Power:\s+(-\d+.\d+)",data,re.S)
    rightshow1 = re.findall(r"Port\s+(\d+).*?RX Power:\s+(-\d+.\d+).*?TX Power:\s+(-\d+.\d+)",data,re.S)
    for i in switchshowdata1:
        for k in rightshow:
            if switchshowdata1[i]["slot"] == k[0] and switchshowdata1[i]["port"] == k[1]:
                switchshowdata1[i]["rx power"] = k[2]
                switchshowdata1[i]["tx power"] = k[3]
    for ii in switchshowdata1:
        for kk in rightshow1:
            if ii == kk[0] and switchshowdata1[ii]["slot"] == "":
                print(switchshowdata1[ii]["slot"])
                switchshowdata1[ii]["rx power"] = kk[1]
                switchshowdata1[ii]["tx power"] = kk[2]
    for zz in switchshowdata1:
        if "rx power" in switchshowdata1[zz].keys():
           pass
        else:
            switchshowdata1[zz]["rx power"] = "none"
            switchshowdata1[zz]["tx power"] = "none"
    file.close()
def Start1():
    file_name1 = str(input(u"请输入需要清洗或最新的文件名:"))
#    file_name2 = input(u"请输入需要比对的文件名")
    dataswitch(file_name1)
    hardware(file_name1)
    Alias(file_name1)
    Zone(file_name1)
    Port_sulv(file_name1)
    Rightshow(file_name1)
def Start2():
    file_name1 = str(input(u"请输入需要比对的老文件名:"))
    dataswitch(file_name1)
    hardware(file_name1)
    Alias(file_name1)
    Zone(file_name1)
    Port_sulv(file_name1)
    Rightshow(file_name1)
def Main():
    list_main = {1:"光交巡检",2:"光交文件数据整理",3:"光交巡检比对",4:"主备光交数据比对",5:"退出"}
    for i in list_main:
        print(i,":",list_main[i])
    print("注：1，光交巡检2，光交文件整理，整理端口比对的数据，zone信息等等，2，光交该月和上月的数据比对情况，3，主备光交链路比对")
    mian_caidan = str(input("请选择："))
    if mian_caidan == "1":
        dome_san.Startsan()
        Main()
    elif mian_caidan == "2":
        Start1()
        print("Index","slot","port","status".ljust(11),"wwn".ljust(24),"tx".ljust(7),"rx".ljust(2),"rx power","tx power","alias".ljust(30),"zoneing")
        for i in switchshowdata1:
            print(i.ljust(5),switchshowdata1[i]["slot"].ljust(4),switchshowdata1[i]["port"].ljust(4),switchshowdata1[i]["status"].ljust(11),switchshowdata1[i]["wwn"].ljust(25),switchshowdata1[i]["tx"].ljust(6),switchshowdata1[i]["rx"].ljust(6),switchshowdata1[i]["rx power"].ljust(6),switchshowdata1[i]["tx power"].ljust(6),switchshowdata1[i]["alias"].ljust(30),switchshowdata1[i]["zonename"])
        Main()
    elif mian_caidan == "3":
        Start1()
        switchshowdata1_one = switchshowdata1.copy()
        hardware1_one = hardware1.copy()
        print(switchshowdata1_one)
        print(hardware1_one)
        switchshowdata1.clear()
        hardware1.clear()
        Start2()
        if hardware1_one == hardware1:
            print(u"硬件比对正常")
        else:
            print(u"硬件或者配置错误，请排查")
        for i in switchshowdata1:
            if switchshowdata1_one[i]["status"] == switchshowdata1[i]["status"]:
                if switchshowdata1_one[i]["wwn"] == switchshowdata1[i]["wwn"]:
                    pass
                else:
                    print(i,"端口wwn从",switchshowdata1[i]["wwn"],"改变为",switchshowdata1_one[i]["wwn"])
                pass
            else:
                print(i,"端口链路比对错误，状态为",switchshowdata1_one[i]["status"],switchshowdata1[i]["wwn"],"变更为",switchshowdata1_one[i]["wwn"])
        Main()
    elif mian_caidan == "4":
        Start1()
        switchshowdata1_one = switchshowdata1.copy()
        hardware1_one = hardware1.copy()
        print(switchshowdata1_one)
        print(hardware1_one)
        switchshowdata1.clear()
        hardware1.clear()
        Start2()
        if hardware1_one == hardware1:
            print(u"硬件比对正常")
        else:
            print(u"硬件或者配置错误，请排查")
        for i in switchshowdata1:
            if switchshowdata1_one[i]["status"] == switchshowdata1[i]["status"]:
                pass
            else:
                print(i,"端口链路比对错误，端口位置不一样",switchshowdata1_one[i]["status"],switchshowdata1[i]["status"])
        Main()
    elif mian_caidan == "5":
        pass
    elif mian_caidan == "":
        Main()
if __name__ == "__main__":
    Main()
#    print(hardware1)
