安装说明：
https://www.jianshu.com/p/8e054ebfe860
# 存储光交业务管理系统
管理存储，光交，服务器，信息收集，分析
python包安装需求：
采用目前最流行的pipenv神器。pipenv=pip + virtualenv,。python的福音。

pip install pipenv

安装完成后进入项目内,直接进行安装。

pipenv check  #对包内的文件进行核对
pipenv install #进行安装
pipenv lock #锁定

核对成功



以下是Pipfile内的文件内容。主要是抓取该文件进行包安装。

[[source]]
url = "https://pypi.python.org/simple" #更新网址，可修改国内源
verify_ssl = true
name = "pypi"
[packages]  #安装包
django = "1.11.6"
celery = "*"
uwsgi = "*"
pymysql = "*"
python-memcached = "*"
#下面全是xadmin需要的插件包
future = "*"
six = "*"
httplib2 = "*"
django-import-export = "*"
xlwt = "*"
django-crispy-forms = "*"
django-formtools = "*"
xlsxwriter = "*"
[dev-packages]#测试的安装包

修改项目的数据库位置：

DATABASES = {
'default': {
    'ENGINE': 'django.db.backends.mysql',
    'NAME': 'app',
    'USER':'root',
    'PASSWORD':'root',
    'HOST':'127.0.0.1',
    'PORT':'3306',
     }
}

包安装完成后首先进入项目内：

#进入虚拟环境
pipenv shell
#像数据库创建表。
python3 manage.py makemigrations
python3 manage.py migrate
#测试运行。
python3 manage.py runserver 127.0.0.1:8000

到此为止基本上能正常使用。

生产环境的安装比较复杂，目前大部分采用nginx + uwsgi + django环境。

我这个系统要求不高，平常访问量最多10人左右。最后采用精简的方式，只采用uwsgi + django的方式进行并发。

uwsgi.ini代码如下：

[uwsgi]
# 项目目录
chdir=/home/manager/manager/
http = :8000
file = manager/wsgi.py
static-map=/static=collectstatic
processes = 4
threads = 10
daemonize=/home/manager/manager/start/uwsgi.log
post-buffering=4096
harakiri=30
enable-threads=true
thunder-lock=true

shell脚本采用最简单的代码：

cd /home/manager/manager/start
uwsgi -i uwsgi.ini
