"""manager URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from work import views
from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
# from django.conf.urls.static import static
from django.conf.urls import include
import work
urlpatterns = [
    url(r'/faults/',views.Faults,name="faults_manager"),
    url(r'/work_manager/',views.Work_manager,name="work_manager"),
    url(r'/work_work_ajax/',views.Work_work_ajax,name="work_work_ajax"),
    url(r'/work_modfiy/',views.Work_modfiy,name="work_modfiy"),
    url(r'/work_upadte_ajax/',views.Work_upadte_ajax,name="work_upadte_ajax"),
]

# static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)