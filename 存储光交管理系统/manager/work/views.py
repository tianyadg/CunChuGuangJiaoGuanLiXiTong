from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.
from work import models
from san.models import Storage,San
from django.template.loader import get_template
from django.shortcuts import render
from django.template import RequestContext, loader
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
import json
# Create your views here.
@login_required(login_url="/login/")
def Faults(request):
    obj = models.Fault.objects.values("FA_time","FA_Reason","ST_storage__ST_ip","ST_san__ip","FA_Grade","FA_Processing","FA_fishtime").all().order_by("FA_time")
    return render(request,"fault.html",{"data":obj})
@login_required(login_url="/login/")
def Work_manager(request):
    obj = models.Work.objects.values("id","WO_time","WO_department","WO_people","WO_textname","WO_storage","WO_text","WO_CAP","WO_CAPname","WO_fishtime").order_by("-WO_time")
    return render(request,"work.html",{"data":obj})
@login_required(login_url="/login/")
def Work_work_ajax(request):
    ret = {"status": True, "error": None, "data": None}
    if request.method == "POST":
        id = request.POST.get("id")
        obj = models.Work.objects.get(id = id)
        ret["data"] = obj.WO_note
    return HttpResponse(json.dumps(ret))
#返回字符串形式 2017-4-5 13:45:45
def Timezhuanhuan(time):
    return str(time.year) + "-" + str(time.month)+ "-" + str(time.day) + " " + str(time.hour) + ":" + str(time.minute) + ":" + str(time.second)

def Work_upadte_ajax(request):
    ret = {"status": True, "error": None, "data": None}
    if request.method == "POST":
        id = request.POST.get("id")
        obj = models.Work.objects.get(id = id)
        ret["WO_time"] = Timezhuanhuan(obj.WO_time)
        ret["WO_department"] = obj.WO_department
        ret["WO_text"] = obj.WO_text
        ret["WO_CAPname"] = obj.WO_CAPname
        ret["WO_CAP"] = obj.WO_CAP
        ret["WO_people"] = obj.WO_people
        ret["WO_textname"] = obj.WO_textname
        ret["WO_storage"] = obj.WO_storage.ST_ip
        ret["WO_fishtime"] =Timezhuanhuan(obj.WO_fishtime)
        ret["WO_note"] = obj.WO_note
        ret["id"] = obj.id
    return HttpResponse(json.dumps(ret))

def Work_modfiy(request):
    ret = {"status": True, "error": None, "data": None}
    if request.method == "POST":
        id = request.POST.get("id")
        WO_CAPname = request.POST.get("WO_CAPname")
        jiesushijian = request.POST.get("jiesushijian")
        xuqiushijian = request.POST.get("xuqiushijian")
        WO_people = request.POST.get("WO_people")
        WO_CAP = request.POST.get("WO_CAP")
        WO_text = request.POST.get("WO_text")
        WO_department = request.POST.get("WO_department")
        WO_textname = request.POST.get("WO_textname")
        WO_storage = request.POST.get("WO_storage")
        editor_id = request.POST.get("editor_id")
        try:
            if id == None:
                obj = models.Storage.objects.filter(ST_ip = WO_storage)
                data = {
                    "WO_CAPname":WO_CAPname,
                    "WO_fishtime":jiesushijian,
                    "WO_time":xuqiushijian,
                    "WO_people" : WO_people,
                    "WO_CAP" : WO_CAP,
                    "WO_text":WO_text,
                    "WO_department" :WO_department,
                    "WO_textname" : WO_textname,
                    "WO_storage" : obj[0],
                    "WO_note" : editor_id,
                }
                # models.Work.objects.create(**data)
                print(data)
            else:
                obj = models.Work.objects.get(id=id)
                obj.WO_CAPname = WO_CAPname
                obj.WO_fishtime = jiesushijian
                obj.WO_time = xuqiushijian
                obj.WO_people = WO_people
                obj.WO_CAP = WO_CAP
                obj.WO_text = WO_text
                obj.WO_department = WO_department
                obj.WO_textname = WO_textname
                obj.save()
        except Exception as e:
            ret["status"] = False
            ret["error"] = "存储信息填写错误"
    print(ret)
    return HttpResponse(json.dumps(ret))