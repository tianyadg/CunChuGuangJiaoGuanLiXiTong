
# Register your models here.
from django.contrib import admin
from work import models
import xadmin
class Faultadmin(object):
    list_display = ("id","FA_time","FA_Reason","FA_Grade","FA_Processing","FA_fishtime","ST_storage","ST_san")
    search_fields = ["FA_Reason","FA_Grade","FA_Processing","FA_fishtime","ST_storage","ST_san"]
    list_filter = ["id","FA_time","FA_Reason","FA_Grade","FA_Processing","FA_fishtime","ST_storage","ST_san"]
class Workadmin(object):
    list_display = ("id","WO_time","WO_department","WO_people","WO_textname","WO_storage","WO_text","WO_CAP","WO_CAPname","WO_fishtime")
    search_fields = ["WO_department","WO_people","WO_textname","WO_storage","WO_text","WO_CAP","WO_CAPname"]
    list_filter = ["id","WO_time","WO_department","WO_people","WO_textname","WO_storage","WO_text","WO_CAP","WO_CAPname","WO_fishtime"]
class Tasksadmin(object):
    list_display = ("id","Ta_times","Ta_text","Ta_statue","Ta_time")
xadmin.site.register(models.Fault,Faultadmin)
xadmin.site.register(models.Work,Workadmin)
xadmin.site.register(models.Tasks,Tasksadmin)