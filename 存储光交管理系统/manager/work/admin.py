from django.contrib import admin

# Register your models here.
from django.contrib import admin
from work import models

@admin.register(models.Fault)
class Faultadmin(admin.ModelAdmin):
    list_display = ("id","FA_time","FA_Reason","FA_Grade","FA_Processing","FA_fishtime","ST_storage","ST_san")
@admin.register(models.Work)
class Workadmin(admin.ModelAdmin):
    list_display = ("id","WO_time","WO_department","WO_people","WO_textname","WO_storage","WO_text","WO_CAP","WO_CAPname","WO_fishtime")
    class Media:
        js = (
            '/static/js/kindeditor-all.js',
            '/static/js/lang/zh_CN.js',
            '/static/js/config.js',
        )
