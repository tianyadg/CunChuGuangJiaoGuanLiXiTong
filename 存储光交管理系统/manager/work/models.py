from django.db import models
from san.models import San
from san.models import Storage
import django.utils.timezone as timezone
# Create your models here.
class Fault(models.Model):
    FA_time = models.DateTimeField(verbose_name="需求创建",blank=True,default = timezone.now)
    FA_Reason = models.CharField(max_length=256,verbose_name="故障原因",blank=True)
    FA_Grade = models.CharField(max_length=256,verbose_name="级别",blank=True)
    FA_Processing =models.CharField(max_length=256,verbose_name="处理过程",blank=True)
    FA_fishtime = models.DateTimeField(verbose_name="结束时间",blank=True,null=True)
    ST_storage = models.ForeignKey(Storage,verbose_name="存储",blank=True,null=True,on_delete=models.SET_NULL)
    ST_san = models.ForeignKey(San,verbose_name="光交",blank=True,null=True,on_delete=models.SET_NULL)
    def __str__(self):
        return self.FA_Reason
    class Meta:
        verbose_name = "故障处理表"
        verbose_name_plural = verbose_name
class Work(models.Model):
    WO_time = models.DateTimeField(verbose_name="需求时间",blank=True,default = timezone.now)
    WO_department = models.CharField(max_length=64,verbose_name="需求部门",blank=True)
    WO_text = models.CharField(max_length=64, verbose_name="需求说明", blank=True)
    WO_CAP = models.CharField(max_length=10, verbose_name="容量需求", blank=True)
    WO_CAPname = models.CharField(max_length=64, verbose_name="容量说明", blank=True)
    WO_textname = models.CharField(max_length=64, verbose_name="业务名称", blank=True)
    WO_people = models.CharField(max_length=64, verbose_name="需求人", blank=True)
    WO_storage = models.ForeignKey(Storage, verbose_name="存储", blank=True, null=True,on_delete=models.SET_NULL)
    WO_fishtime =models.DateTimeField(verbose_name="完成时间",blank=True,null=True)
    WO_note = models.TextField(verbose_name="详细信息",blank=True,null=True)
    def __str__(self):
        return self.WO_textname
    class Meta:
        verbose_name = "需求简表"
        verbose_name_plural = verbose_name
class Tasks(models.Model):
    STATUS_CHOICES =(
        ("0","成功"),
        ("1","失败"),
        ("2","运行中"),
    )
    Ta_times = models.DateTimeField(verbose_name="任务创建时间",blank=True,default = timezone.now)
    Ta_text = models.CharField(max_length=999,verbose_name="任务详情",blank=True)
    Ta_statue = models.CharField(max_length=4,verbose_name="任务状态",blank=True,choices=STATUS_CHOICES)
    Ta_time =  models.CharField(max_length=10,verbose_name="执行时间",blank=True)
    def __str__(self):
        return self.Ta_text
    class Meta:
        verbose_name = "任务情况"
        verbose_name_plural = verbose_name