from django.db import models

# Create your models here

from san.models import Storage
class RAIDgroup(models.Model):
    rid = models.IntegerField(verbose_name="ID号码", null=True, blank=True)
    raidname = models.CharField(max_length=20,verbose_name="raid名称",blank=True,null=True)
    raidtype = models.CharField(max_length=20,verbose_name="raid类型",blank=True,null=True)
    raidsize = models.CharField(max_length=20,verbose_name="raid大小",blank=True,null=True)
    radifree = models.CharField(max_length=20,verbose_name="剩余类型",blank=True,null=True)
    raidnote = models.CharField(max_length=100,verbose_name="备注",blank=True,null=True)
    storage = models.ForeignKey(Storage, verbose_name="存储信息", null=True, blank=True,on_delete=models.SET_NULL)

    def __str__(self):
        return self.raidname
    class Meta:
        verbose_name = "存储raid组"
        verbose_name_plural = verbose_name
class DISK(models.Model):
    did = models.IntegerField(verbose_name="ID号码", null=True, blank=True)
    disksize = models.CharField(max_length=20,verbose_name="硬盘大小",blank=True,null=True)
    diskslot = models.CharField(max_length=20,verbose_name="硬盘槽位",blank=True,null=True)
    status = models.CharField(max_length=20,verbose_name="硬盘可用状态",blank=True,null=True)
    disktype = models.CharField(max_length=20,verbose_name="硬盘类型",blank=True,null=True)
    datatype = models.CharField(max_length=20,verbose_name="数据类型",blank=True,null=True)
    diskstatus = models.CharField(max_length=20,verbose_name="硬盘状态",blank=True,null=True)
    disknote = models.CharField(max_length=100,verbose_name="备注",blank=True,null=True)
    raidgroup = models.ForeignKey(RAIDgroup,verbose_name="所属于raid组",blank=True,null=True,on_delete=models.SET_NULL)
    storage = models.ForeignKey(Storage, verbose_name="存储信息", null=True, blank=True,on_delete=models.SET_NULL)
    def __str__(self):
        return self.disktype
    class Meta:
        verbose_name = "存储硬盘信息"
        verbose_name_plural = verbose_name
class STPORT(models.Model):
    pid = models.IntegerField( verbose_name="ID号码", null=True, blank=True)
    portname = models.CharField(max_length=100,verbose_name="端口名称",null=True,blank=True)
    porttype = models.CharField(max_length=100,verbose_name="端口类型",null=True,blank=True)
    Notes = models.CharField(max_length=9999,verbose_name="备注",null=True,blank=True)
    storage = models.ForeignKey(Storage,verbose_name="存储信息", null=True, blank=True,on_delete=models.SET_NULL)
    def __str__(self):
        return self.portname
    class Meta:
        verbose_name = "存储端口"
        verbose_name_plural = verbose_name
class STPORTgroup(models.Model):
    pid = models.IntegerField(verbose_name="ID号码",null=True,blank=True)
    portgroupname = models.CharField(max_length=100,verbose_name="端口组名称",null=True,blank=True)
    Notes = models.CharField(max_length=9999,verbose_name="备注",null=True,blank=True)
    storage = models.ForeignKey(Storage,verbose_name="存储信息", null=True, blank=True,on_delete=models.SET_NULL)
    port = models.ManyToManyField(STPORT,verbose_name="端口",blank=True)
    def __str__(self):
        return self.portgroupname
    class Meta:
        verbose_name = "存储端口组"
        verbose_name_plural = verbose_name
class STLUN(models.Model):
    lid = models.IntegerField(verbose_name="ID号码", null=True, blank=True)
    lunpname = models.CharField(max_length=100, verbose_name="lun名称", null=True, blank=True)
    lunsize = models.CharField(max_length=100, verbose_name="lun大小", null=True, blank=True)
    luntype = models.CharField(max_length=100, verbose_name="lun类型", null=True, blank=True)
    Notes = models.CharField(max_length=9999, verbose_name="备注", null=True, blank=True)
    storage = models.ForeignKey(Storage, verbose_name="存储信息", null=True, blank=True,on_delete=models.SET_NULL)

    def __str__(self):
        return self.lunpname

    class Meta:
        verbose_name = "存储LUN"
        verbose_name_plural = verbose_name
class STLUNgroup(models.Model):
    lid = models.IntegerField( verbose_name="ID号码", null=True, blank=True)
    lungroupname = models.CharField(max_length=100,verbose_name="lun组名称",null=True,blank=True)
    Notes = models.CharField(max_length=9999,verbose_name="备注",null=True,blank=True)
    storage = models.ForeignKey(Storage,verbose_name="存储信息", null=True, blank=True,on_delete=models.SET_NULL)
    lun = models.ManyToManyField(STLUN,verbose_name="LU",blank=True)
    def __str__(self):
        return self.lungroupname
    class Meta:
        verbose_name = "存储LUN组"
        verbose_name_plural = verbose_name
class STHOST(models.Model):
    hid = models.IntegerField( verbose_name="ID号码", null=True, blank=True)
    hostname = models.CharField(max_length=100,verbose_name="主机名称",null=True,blank=True)
    hosttype = models.CharField(max_length=100,verbose_name="主机类型",null=True,blank=True)
    Notes = models.CharField(max_length=9999,verbose_name="备注",null=True,blank=True)
    storage = models.ForeignKey(Storage,verbose_name="存储信息", null=True, blank=True,on_delete=models.SET_NULL)
    def __str__(self):
        return self.hostname
    class Meta:
        verbose_name = "存储主机"
        verbose_name_plural = verbose_name
class STHOSTgroup(models.Model):
    hid = models.IntegerField( verbose_name="ID号码", null=True, blank=True)
    hostgroupname = models.CharField(max_length=100,verbose_name="主机组名称",null=True,blank=True)
    hostgroupapp = models.CharField(max_length=300,verbose_name="主机组业务名称",null=True,blank=True)
    Notes = models.CharField(max_length=9999,verbose_name="备注",null=True,blank=True)
    storage = models.ForeignKey(Storage,verbose_name="存储信息", null=True, blank=True,on_delete=models.SET_NULL)
    host = models.ManyToManyField(STHOST, verbose_name="主机", blank=True)
    def __str__(self):
        return self.hostgroupname
    class Meta:
        verbose_name = "存储主机组"
        verbose_name_plural = verbose_name

class STMAP(models.Model):
    mid = models.IntegerField( verbose_name="ID号码", null=True, blank=True)
    mapname = models.CharField(max_length=100,verbose_name="map名称",null=True,blank=True)
    manyportgroup = models.ManyToManyField(STPORTgroup, verbose_name="端口组",blank=True)
    manyport = models.ManyToManyField(STPORT,verbose_name="端口",blank=True)
    manyhostgroup = models.ManyToManyField(STHOSTgroup,verbose_name="主机组",blank=True)
    manyhost = models.ManyToManyField(STHOST,verbose_name="主机",blank=True)
    manylungroup = models.ManyToManyField(STLUNgroup,verbose_name="lun组",blank=True)
    manylun = models.ManyToManyField(STLUN,verbose_name="lun",blank=True)
    app = models.CharField(max_length=100,verbose_name="业务名称",blank=True)
    Notes = models.CharField(max_length=9999,verbose_name="备注",blank=True)
    totalcap = models.CharField(max_length=20,verbose_name="已用容量",blank=True)
    storage = models.ForeignKey(Storage,verbose_name="存储信息",null=True, blank=True,on_delete=models.SET_NULL)
    def __str__(self):
        return self.app
    class Meta:
        verbose_name = "存储聚合组"
        verbose_name_plural = verbose_name
