import re
def Re(res,total,id = "1"):
    if id == "1":
        data = re.findall(str(res),total)
    if id == "2":
        data = re.findall(str(res),total,re.S)
    return data
class Sanimport():
    def __init__(self,data):
        self.data = data
#        print(self.data)
    def Index(self):
        a = Re(r'(\d+\s+\d+\s+\d+|\d+\s+\d+)\s+(\S{6})\s+\S{2}\s+(\S{2,3})\s+(\S+)\s+FC\s+(\s|\S+\s+..:..:..:..:..:..:..:..)',self.data)
        Index ={}
        for i in a:
            port_mess = i[0].split(" ")
            while "" in port_mess:
                port_mess.remove("")
            Index[port_mess[0]] = {}
            wwn_mess = i[4].split(" ")
            while "" in wwn_mess:
                wwn_mess.remove("")
            while "\n" in wwn_mess:
                wwn_mess.remove("\n")
            if len(port_mess) == 3:
                Index[port_mess[0]]["slot"] = port_mess[1]
                Index[port_mess[0]]["port"] = port_mess[2]
                Index[port_mess[0]]["fcid"] = i[2]
                Index[port_mess[0]]["speed"] = i[3]
                if len(wwn_mess) ==2 :
                    Index[port_mess[0]]["type"] = wwn_mess[0]
                    Index[port_mess[0]]["wwn"] = wwn_mess[1]
                else:
                    Index[port_mess[0]]["type"] = ""
                    Index[port_mess[0]]["wwn"] = ""
            if len(port_mess) == 2:
                Index[port_mess[0]]["slot"] = ""
                Index[port_mess[0]]["port"] = port_mess[1]
                Index[port_mess[0]]["fcid"] = i[2]
                Index[port_mess[0]]["speed"] = i[3]
                if len(wwn_mess) ==2 :
                    Index[port_mess[0]]["type"] = wwn_mess[0]
                    Index[port_mess[0]]["wwn"] = wwn_mess[1]
                else:
                    Index[port_mess[0]]["type"] = ""
                    Index[port_mess[0]]["wwn"] = ""
        return Index
    def Porterr(self):
        data_indx = self.Index()
        a = Re(r"(\d+):\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+",self.data)
        for i in data_indx:
            for ii in a:
                if i == ii[0]:
                    data_indx[i]["tx"] = ii[1]
                    data_indx[i]["rx"] = ii[2]
                    data_indx[i]["enc_in"] = ii[3]
                    data_indx[i]["crc_err"] = ii[4]
                    data_indx[i]["crc_g_eof"] = ii[5]
                    data_indx[i]["two_shrt"] = ii[6]
                    data_indx[i]["two_long"] = ii[7]
                    data_indx[i]["bad_eof"] = ii[8]
                    data_indx[i]["enc_out"] = ii[9]
                    data_indx[i]["disc_c3"] = ii[10]
                    data_indx[i]["link_fail"] = ii[11]
                    data_indx[i]["loss_sync"] = ii[12]
                    data_indx[i]["loss_sig"] = ii[13]
                    data_indx[i]["frjt"] = ii[14]
                    data_indx[i]["fbsy"] = ii[15]
                    data_indx[i]["timeout_tx"] = ii[16]
                    data_indx[i]["timeout_rx"] = ii[17]
        return data_indx
    def Sfpshow(self):
        a = Re(r"Slot\s+(\d{1,2})/Port\s+(\d+).*?RX Power:\s+(-\d+.\d+).*?TX Power:\s+(-\d+.\d+)",self.data,"2")
        if a == []:
            a = Re(r"Port\s+(\d+):.*?RX Power:\s+(-\d+.\d+).*?TX Power:\s+(-\d+.\d+)", self.data,"2")
        Indextotal = self.Porterr()
        if len(a[0]) == 3:
            for i in Indextotal:
                for k in a:
                    if i == k[0]:
                        Indextotal[i]["RX_Power"] = k[1]
                        Indextotal[i]["TX_Power"] = k[2]
        elif len(a[0]) == 4:
            for i in Indextotal:
                for k in a:
                    if Indextotal[i]["slot"] == k[0] and Indextotal[i]["port"] == k[1]:
                        Indextotal[i]["RX_Power"] = k[2]
                        Indextotal[i]["TX_Power"] = k[3]
        # print(Indextotal)
        for i in Indextotal:
            Indextotal[i].setdefault("RX_Power")
            Indextotal[i].setdefault("TX_Power")
            Indextotal[i].setdefault("tx")
            Indextotal[i].setdefault("rx")
            Indextotal[i].setdefault("enc_in")
            Indextotal[i].setdefault("crc_err")
            Indextotal[i].setdefault("crc_g_eof")
            Indextotal[i].setdefault("two_shrt")
            Indextotal[i].setdefault("two_long")
            Indextotal[i].setdefault("bad_eof")
            Indextotal[i].setdefault("enc_out")
            Indextotal[i].setdefault("disc_c3")
            Indextotal[i].setdefault("link_fail")
            Indextotal[i].setdefault("loss_sig")
            Indextotal[i].setdefault("frjt")
            Indextotal[i].setdefault("fbsy")
            Indextotal[i].setdefault("timeout_tx")
            Indextotal[i].setdefault("timeout_rx")

        return Indextotal
    def Hardware(self):
        slot = Re(r"((\d+\s+(\w+\s\w+)\s+\d+\s+((ENABLED)|(VACANT)))|(\d+\s+\w+\s+((ENABLED)|(VACANT))))",self.data)
        power = Re(r"Power Supply #(\d+)\s+is\s+(\w+)", self.data)
        fan = Re(r"Fan\s+(\d+)\s+is\s+(\w+)", self.data)
        mem = Re(r"Mem:\s+\d+\s+\d+\s+(\d+)", self.data)
        card =  Re(r"\d+\s+\S+\s+FOS\s+\S+\s+\w+", self.data)
        if int(mem[0]) > 100000:
            mem = "True"
        hardwaredict = {
            "slot":slot,
            "power":power,
            "fan":fan,
            "card":card,
            "mem":mem,
        }
        return hardwaredict
    def Alias(self):
        a = Re(r'alias:\s(.*)\n\s+(..:..:..:..:..:..:..:..)',self.data)
        print(a)
    def Zone(self):
        pass
    