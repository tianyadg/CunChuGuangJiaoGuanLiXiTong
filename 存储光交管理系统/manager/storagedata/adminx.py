import xadmin

from storagedata import models
# Register your models here.

class STMAPadmin(object):
    list_display = ("id","app","mapname","Notes","storage")
    list_editable = ["app"]
    search_fields = ["id","app","mapname"]
    list_filter = ["storage"]
class sthostgroupadmin(object):
    list_display = ("id","hostgroupname","Notes","storage")
    list_filter = ["storage"]
class sthostadmin(object):
    list_display = ("id","hostname","hosttype","Notes","storage")
    list_filter = ["storage"]
class stlungroupadmin(object):
    list_display = ("id","lungroupname","Notes","storage")
    list_filter = ["storage"]
class sthlundmin(object):
    list_display = ("id","lunpname","lunsize","Notes","storage")
    list_filter = ["storage"]
class stpostgroupadmin(object):
    list_display = ("id","portgroupname","Notes","storage")
    list_filter = ["storage"]
class stportadmin(object):
    list_display = ("id","portname","porttype","Notes","storage")
    list_filter = ["storage"]
xadmin.site.register(models.STMAP,STMAPadmin)
xadmin.site.register(models.STHOSTgroup,sthostgroupadmin)
xadmin.site.register(models.STHOST,sthostadmin)
xadmin.site.register(models.STLUNgroup,stlungroupadmin)
xadmin.site.register(models.STLUN,sthlundmin)
xadmin.site.register(models.STPORTgroup,stpostgroupadmin)
xadmin.site.register(models.STPORT,stportadmin)