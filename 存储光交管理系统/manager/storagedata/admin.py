from django.contrib import admin

from storagedata import models
# Register your models here.
@admin.register(models.STMAP)
class STMAPadmin(admin.ModelAdmin):
    list_display = ("id","app","mapname","Notes","storage")
    list_editable = ["app"]
@admin.register(models.STHOSTgroup)
class sthostgroupadmin(admin.ModelAdmin):
    list_display = ("id","hostgroupname","Notes","storage")
@admin.register(models.STHOST)
class sthostadmin(admin.ModelAdmin):
    list_display = ("id","hostname","hosttype","Notes","storage")
@admin.register(models.STLUNgroup)
class stlungroupadmin(admin.ModelAdmin):
    list_display = ("id","lungroupname","Notes","storage")
@admin.register(models.STLUN)
class sthlundmin(admin.ModelAdmin):
    list_display = ("id","lunpname","lunsize","Notes","storage")
@admin.register(models.STPORTgroup)
class stpostgroupadmin(admin.ModelAdmin):
    list_display = ("id","portgroupname","Notes","storage")
@admin.register(models.STPORT)
class stportadmin(admin.ModelAdmin):
    list_display = ("id","portname","porttype","Notes","storage")
