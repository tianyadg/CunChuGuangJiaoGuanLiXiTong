from django.apps import AppConfig


class StoragedataConfig(AppConfig):
    name = 'storagedata'
