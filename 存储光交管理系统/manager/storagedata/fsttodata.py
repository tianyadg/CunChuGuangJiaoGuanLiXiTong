#!/usr/bin/envpython
#-*-coding:utf-8-*-
import re
from storagedata import models
from san.models import Wwn,Storage
import logging
from scriptd.tasks import Task
logger = logging.getLogger('django')
def Re(res,total,id = "1"):
    if id == "1":
        data = re.findall(str(res),total)
    if id == "2":
        data = re.findall(str(res),total,re.S)
    return data
from scriptd.fsjinfo import fsjinfo
from scriptd.towwn import WWn
#存储id号码跟日志
def Fsttodata(id,log):
    #1为默认，2为DX8700S3
    task = Task()
    stname = Storage.objects.get(id = id)
    task.start(str("富士通存储" + stname.ST_prodect + "-" + stname.ST_ip + "开始清洗数据"))
    if stname.ST_prodect == "DX8700S3":
        dome = fsjinfo(log,"2")
    else:
        dome = fsjinfo(log)

    lun = dome.lun()
    juhezu = dome.juhemess()
    hostgroup = dome.Hostgroup()
    disk = dome.diskinfo()
    raid = dome.raidinfo()
    # 端口信息
    port_group = dome.portgroup()
    for i in port_group[0]:
        data = {
            "portname": str(i),
            "storage_id": id,
        }
        if not models.STPORT.objects.filter(storage_id=id,portname=str(i)):
            obj = models.STPORT.objects.create(**data)
            wwn = WWn(port_group[0][i])
            objwwn = Wwn.objects.update_or_create(wwn=wwn)
            objwwn[0].storage_wwn_id = id
            objwwn[0].STport_id = obj.id
            objwwn[0].wwnname = i
            objwwn[0].save()
    logger.info("fst-"+ id +"-port_finish")
    #端口组
    for i in port_group[1]:
        data = {
            "portgroupname": str(i),
            "storage_id": id,
        }
        if not models.STPORTgroup.objects.filter(storage_id=id,portgroupname=str(i)):
            obj = models.STPORTgroup.objects.create(**data)
            for k in port_group[1][i]:
                obj.port.add(*(models.STPORT.objects.filter(storage_id=id,portname=str(k))))
    #主机
    for i in hostgroup[0]:
        data = {
                    "hostname": i[0],
                    "hosttype": i[2],
                    "storage_id": id,
                }
        if not models.STHOST.objects.filter(hostname=i[0],storage_id=id,hosttype=i[2]):
            obj = models.STHOST.objects.create(**data)
            wwn = WWn(i[1])
            objhostwwn = Wwn.objects.update_or_create(wwn=wwn)
            objhostwwn[0].storage_wwn_id = id
            objhostwwn[0].SThost.add(obj)
            objhostwwn[0].save()
    logger.info("fst-" + id + "-host_finish")
    # 添加主机组
    for i in hostgroup[1]:
        data = {
                    "hostgroupname": i,
                    "storage_id": id,
                }
        obj = models.STHOSTgroup.objects.filter(storage_id=id, hostgroupname=i).first()
        if not obj:
            obj = models.STHOSTgroup.objects.create(**data)
        obj.host.clear()
        for k in hostgroup[1][i]:
            obj.host.add(*(models.STHOST.objects.filter(storage_id=id,hostname=k)))
    #lun
    for i in lun[0]:
        data = {
            "lid": i[0],
            "lunpname": i[1],
            "lunsize": str(round(int(i[5])/1024,3)),
            # "lunsize": str(round(int(i[4])/1024,3)),
            "luntype": "",
            "storage_id": id,
        }
        obj = models.STLUN.objects.filter(storage_id=id, lid=i[0]).first()
        if not obj:
            obj = models.STLUN.objects.create(**data)
        else:
            obj.lunpname = i[1]
            obj.lunsize = str(round(int(i[5])/1024,3))
            obj.save()
    #lun组
    for i in lun[1]:
        data = {
            "lid": i,
            "lungroupname": lun[1][i]["name"],
            "storage_id": id,
        }
        obj = models.STLUNgroup.objects.filter(storage_id=id, lid=i,lungroupname= lun[1][i]["name"]).first()
        if not obj:
            obj = models.STLUNgroup.objects.create(**data)
        obj.lun.clear()
        for i in lun[1][i]["LUNID"]:
            obj.lun.add(*(models.STLUN.objects.filter(lid=int(i), storage_id=id)))
    logger.info("fst-" + id + "-lun_finish")
    # 聚合信息
    #id 端口组  id 主机组 id  lun组
    for i in juhezu:
        data = {
            "mapname":i[3],
            "storage_id":id,
            "Notes":i,
        }
        obj = models.STMAP.objects.filter(storage_id=id,Notes=i)
        if not obj:
            obj = models.STMAP.objects.create(**data)
            obj.manyhostgroup.add(*(models.STHOSTgroup.objects.filter(storage_id=id,hostgroupname=i[3])))
            obj.manyportgroup.add(*(models.STPORTgroup.objects.filter(storage_id=id,portgroupname=i[1])))
            obj.manylungroup.add(*(models.STLUNgroup.objects.filter(storage_id=id,lid=i[4])))
        else:
            obj[0].mapname = i[3]
            obj[0].manyhostgroup.clear()
            obj[0].manylungroup.clear()
            obj[0].manyportgroup.clear()
            obj[0].manyhostgroup.add(*(models.STHOSTgroup.objects.filter(storage_id=id,hostgroupname=i[3])))
            obj[0].manyportgroup.add(*(models.STPORTgroup.objects.filter(storage_id=id,portgroupname=i[1])))
            obj[0].manylungroup.add(*(models.STLUNgroup.objects.filter(storage_id=id,lid=i[4])))
            obj[0].save()
    logger.info("fst-" + id + "-map_finish")
    #硬盘信息表
    print("map_finish")
    for d in disk:
        data = {
            "diskslot":d[0],
            "disktype":d[3],
            "status":d[1],
            "disksize":d[2],
            "datatype":d[6],
            "diskstatus":d[4],
            "storage_id":id
        }
        obj = models.DISK.objects.filter(storage_id =id,diskslot =d[0]).first()
        if not obj:
            obj = models.DISK.objects.create(**data)
        else:
            obj.disktype = d[3]
            obj.status = d[1]
            obj.disksize = d[2]
            obj.datatype = d[6]
            obj.diskstatus = d[4]
            obj.save()
    logger.info("fst-" + id + "-disk_finish")
    #raid组信息
    for r in raid:
        data = {
            "rid":r[0],
            "raidname":r[1],
            "raidtype":r[2],
            "raidsize":r[3],
            "radifree":r[4],
            "storage_id": id
        }
        obj = models.RAIDgroup.objects.filter(storage_id =id,rid = r[0]).first()
        if not obj:
            obj = models.RAIDgroup.objects.create(**data)
        else:
            obj.raidname = r[1]
            obj.raidtype = r[2]
            obj.raidsize = r[3]
            obj.radifree = r[4]
            obj.save()
    logger.info("fst-" + id + "-finish")
    task.end()