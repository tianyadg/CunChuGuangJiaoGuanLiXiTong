#!/usr/bin/envpython
#-*-coding:utf-8-*-
# sys.path.append('../manager/')
# os.environ['DJANGO_SETTINGS_MODULE']='manager.settings'
# from manager import settings
# import django
# django.setup()
from scriptd.tasks import Task
from san import models
from san.models import Wwn
# 传送存储ID号码及文本信息
from scriptd.towwn import WWn
# obj = models.Doment.objects.filter(storage=21).first()
from scriptd.huaweistorage import Huawei
from storagedata import models
import logging
logger = logging.getLogger('django')

def Huaweitodata(id,text):
    task = Task()
    task.start(str("华为存储" + id + "-" + "开始刷新数据"))
    a = Huawei(text)
    #端口添加-已完成
    portdata = a.portdata()
    hostdata = a.host()
    lundata = a.lundata()
    mapping = a.maping()
    for i in portdata:
        if not models.STPORT.objects.filter(storage_id = id,portname = i[0]):
            pdata = {
                "portname": i[0],
                "storage_id":id,
            }
            objport = models.STPORT.objects.create(**pdata)
            wwn = WWn(i[1])
            objwwn = Wwn.objects.update_or_create(wwn = wwn)
            objwwn[0].STport = objport
            objwwn[0].storage_wwn_id = id
            objwwn[0].name = i[0]
            objwwn[0].save()
    logger.info("huawei-" + id + "-port_finish")
    #添加主机：


    host = hostdata[1]
    for i in host:
        if not models.STHOST.objects.filter(storage_id=id, hid=i, hostname=host[i]["name"]):
            data ={
                "hid":i,
                "hostname":host[i]["name"],
                "storage_id":id,
            }
            obj = models.STHOST.objects.create(**data)
            for w in host[i]["wwn"]:
                wwn = WWn(w)
                objhostwwn = Wwn.objects.update_or_create(wwn=wwn)
                objhostwwn[0].storage_wwn_id = id
                objhostwwn[0].save()
                objhostwwn[0].SThost.add(obj)
    logger.info("huawei-" + id + "-host_finish")
    #添加主机组

    hostgroup = hostdata[0]
    for i in hostgroup:
        data ={
                    "hid":i,
                    "hostgroupname":hostgroup[i]["name"],
                    "storage_id":id,
                }
        if not models.STHOSTgroup.objects.filter(storage_id=id,hid=i,hostgroupname=hostgroup[i]["name"]):
            obj = models.STHOSTgroup.objects.create(**data)
            for i in hostgroup[i]["HOSTID"]:
                obj.host.add(*(models.STHOST.objects.filter(hid=i,storage_id=id)))
    logger.info("huawei-" + id + "-host_finish")
            # LUN信息收集

    lun = lundata[1]
    for i in lun:
        data = {
            "lid": i,
            "lunpname": lun[i]["name"],
            "lunsize": str(round(int(lun[i]["size"])/1024/1024,3)),
            "luntype": lun[i]["type"],
            "storage_id": id,
        }
        if not models.STLUN.objects.filter(storage_id=id, lid=i, lunpname=lun[i]["name"]):
            obj = models.STLUN.objects.create(**data)
    logger.info("huawei-" + id + "-lun_finish")
    #LUN组
    lungroup = lundata[0]
    for i in lungroup:
        data ={
                    "lid":i,
                    "lungroupname":lungroup[i]["name"],
                    "storage_id":id,
                }
        if not models.STLUNgroup.objects.filter(storage_id=id,lid=i,lungroupname=lungroup[i]["name"]):
            obj = models.STLUNgroup.objects.create(**data)
            for i in lungroup[i]["LUNID"]:
                obj.lun.add(*(models.STLUN.objects.filter(lid=i,storage_id=id)))

    logger.info("huawei-" + id + "-lungroup_finish")
    #mapping信息
    # id name 主机组-端口组-lun组


    for i in mapping:
        data = {
            "mid":i[0],
            "mapname":i[1],
            "storage_id":id,
        }
        if not models.STMAP.objects.filter(mid=i[0],mapname=i[1],storage_id=id):
            obj = models.STMAP.objects.create(**data)
            obj.manylungroup.add(*(models.STLUNgroup.objects.filter(lid=i[4],storage_id=id)))
            obj.manyhostgroup.add(*(models.STHOSTgroup.objects.filter(hid=i[2],storage_id=id)))
# Huaweitodata(21,obj.text)
    logger.info("huawei-" + id + "-mapping_finish")
    #添加硬盘
    for i in a.disk():
        data = {
            "diskslot":i[0],
            "diskstatus":i[2],
            "status":i[1],
            "disknote":i[4],
            "disksize":i[3],
            "storage_id": id
        }
        obj = models.DISK.objects.filter(storage_id=id,diskslot = i[0]).first()
        if not obj:
            obj =  models.DISK.objects.create(**data)
        else:
            obj.diskstatus = i[2]
            obj.status = i[1]
            obj.disknote = i[4]
            obj.disksize = i[3]
            obj.save()
    logger.info("huawei-" + id + "-disk_finish")
    task.end()