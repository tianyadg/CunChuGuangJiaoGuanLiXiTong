# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-01-03 08:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('storagedata', '0010_auto_20180103_1543'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stmap',
            name='totalcap',
            field=models.CharField(blank=True, max_length=20, verbose_name='已用容量'),
        ),
    ]
