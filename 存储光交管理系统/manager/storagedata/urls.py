"""manager URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
# from django.conf.urls.static import static
from django.conf.urls import include
from storagedata import views
from django.views.decorators.cache import cache_page
urlpatterns = [
    # url(r'/faults/',views.Faults,name="faults_manager"),
    url(r"/(?P<storage_id>[0-9]*)/",views.Storagemessages,name="storagemessages"),
    url(r"/storage_one_update_ajax/", views.Storage_one_update_ajax, name="storage_one_update_ajax"),
    url(r"/storage_one_delete_ajax/", views.Storage_one_delete_ajax, name="storage_one_delete_ajax"),
    url(r"/storage_update_ajax/",views.Storage_update_ajax,name="storage_update_ajax"),
    url(r"/storagetable_ajax/",views.Storagetable_ajax,name="storagetable_ajax"),
    url(r"/storage_num_update_ajax/",views.Storage_num_update_ajax,name="storage_num_update_ajax"),
    url(r"/storage_app_modfiy/",views.Storage_app_modfiy,name="storage_app_modfiy"),
    url(r"/storage_stmap_ajax/",cache_page(60 * 5)(views.storage_stmap_ajax),name="storage_stmap_ajax"),
    url(r"/stroage_mess_host_ajax/",cache_page(60 * 5)(views.stroage_mess_host_ajax),name="stroage_mess_host_ajax"),
    url(r"/storage_port_ajax/",cache_page(60 * 5)(views.storage_port_ajax),name="storage_port_ajax"),
    url(r"/storage_lun_ajax/",cache_page(60 * 5)(views.storage_lun_ajax),name="storage_lun_ajax"),
    url(r"/storage_disk_ajax/",cache_page(60 * 5)(views.storage_disk_ajax),name="storage_disk_ajax"),
    url(r"/storage_raid_ajax/",cache_page(60 * 5)(views.storage_raid_ajax),name="storage_raid_ajax"),
    url(r"/storage_work_ajax/",cache_page(60 * 5)(views.storage_work_ajax),name="storage_work_ajax"),
    url(r"/storagetables/",cache_page(60 * 5)(views.Storagetables),name="storagetables"),
    url(r"/storage_stmap_rili_ajax/", cache_page(60 * 5)(views.storage_stmap_rili_ajax), name="storage_stmap_rili_ajax"),
    url(r"/stroage_mess_hostrili__ajax/", cache_page(60 * 5)(views.stroage_mess_host_rili_ajax), name="stroage_mess_host_rili_ajax"),
    url(r"/storage_lun_rili_ajax/", cache_page(60 * 5)(views.storage_lun_rili_ajax), name="storage_lun_rili_ajax"),
]

# static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)