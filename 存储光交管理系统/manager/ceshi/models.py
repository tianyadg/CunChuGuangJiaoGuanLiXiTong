# -*- coding: utf-8 -*-
from django.db import models

class A(models.Model):
    name = models.CharField(max_length=100)
    class Meta:
        verbose_name = u"A等级"
        verbose_name_plural = verbose_name
        def __str__(self):
            return self.name
class B(models.Model):
    name = models.CharField(max_length=100)
    a    = models.ForeignKey(A,blank=True,null=True,on_delete=models.SET_NULL )
    text = models.TextField(blank=True,verbose_name="测试")
    class Meta:
        verbose_name = u"B等级"
        verbose_name_plural = verbose_name
