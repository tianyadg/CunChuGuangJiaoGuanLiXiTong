from django.contrib import admin
from ceshi import models
# Register your models here.

class BInline(admin.StackedInline):
    model = models.B
@admin.register(models.A)
class Aadmin(admin.ModelAdmin):
    list_display = ["name",]
    inlines = [BInline, ]

class badmin(admin.ModelAdmin):
    list_display = ['id',"name"]

admin.site.register(models.B, badmin)

