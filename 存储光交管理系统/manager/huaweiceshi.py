#!/usr/bin/envpython
#-*-coding:utf-8-*-
import os
import sys

sys.path.append('../manager/')
os.environ['DJANGO_SETTINGS_MODULE']='manager.settings'
import django
django.setup()
from san import models
#传送存储ID号码及文本信息
from scriptd.towwn import WWn
obj = models.Doment.objects.filter(storage=20).first()
from scriptd.废弃.huaweistorage import Huawei

def Huaweitodata(id,text):
    a = Huawei(text)
    mapping = a.maping()
    lundata = a.lundata()
    hostdata = a.host()
    portdata = a.portdata()
    # for i in mapping:
    #     print(hostdata[0][i[0]]["name"])
    #    print(hostdata[0])主机组信息表{'6': {'HOSTID': ['40'], 'name': 'ABM_JF02'}
    #    print(hostdata[1])主机信息表{'4': {'wwn': ['21000024ff11d82e', '21000024ff11d89d'], 'name': 'NWYpool05'},

    #聚合  主机组0  端口组1  lun组2
    obj = models.Hostgroup.objects.filter(storage_id=id)
    for i in mapping:
        objnone = models.Hostgroup.objects.filter(storage_id=id,hostgroupname=hostdata[0][i[0]]["name"],lungroupname=lundata[0][i[2]]["name"])
        if not objnone:
            data = {
                "name": str(""),
                "hostgroupname":hostdata[0][i[0]]["name"],
                "portgroupname": "",
                "lungroupname":lundata[0][i[2]]["name"],
                "storage_id": id,
            }
            try:
                models.Hostgroup.objects.create(**data)
            except Exception as e:
                print(i,e)
    #添加主机信息
    #    print(hostdata[0])主机组信息表{'6': {'HOSTID': ['40'], 'name': 'ABM_JF02'}
    #    print(hostdata[1])主机信息表{'4': {'wwn': ['21000024ff11d82e', '21000024ff11d89d'], 'name': 'NWYpool05'},
    for i in hostdata[1]:
        if not models.Storagehost.objects.filter(storage_id=id,hostname=hostdata[1][i]["name"]):
            data = {
                "hostname": hostdata[1][i]["name"],
                "hosttype": "",
                "storage_id":id,
            }
            a = models.Storagehost.objects.create(**data)
            for w in hostdata[1][i]["wwn"]:
                wwn = WWn(w)
                objhostwwn = models.Wwn.objects.update_or_create(wwn=wwn)
                objhostwwn[0].storage_wwn_id = id
                objhostwwn[0].save()
                objhostwwn[0].hostgroup_wwn.add(a)
    #端口添加-已完成
    for i in portdata:
        if not models.StoragePort.objects.filter(storage_id = id,name = i[0]):
            pdata = {
                "name": i[0],
                "storage_id":id,
            }
            objport = models.StoragePort.objects.create(**pdata)
            wwn = WWn(i[1])
            objwwn = models.Wwn.objects.update_or_create(wwn = wwn)
            objwwn[0].storageport_wwn_id = objport.id
            objwwn[0].storage_wwn_id = id
            objwwn[0].name = i[0]
            objwwn[0].save()
Huaweitodata(20,obj.text)
