#!/usr/bin/env python
from gevent import monkey;
#携程并发，太慢
monkey.patch_all()
from gevent import wsgi
from manager.wsgi import application
HOST = '127.0.0.1'
PORT = 8000
# set spawn=None for memcache
wsgi.WSGIServer((HOST, PORT), application).serve_forever()