__author__ = 'l4537'
#coding:utf-8
import time,os,re,sys
from san.sanbokescript import Sanmessages
from san.models import Sanport,Doment,Sanlog_clean
from scriptd.tasks import Task
def Borde_huitu(id):
    d = Doment.objects.filter(san_id = id).order_by("-dotime").first()
    # for d in domxinxi:
    a = Sanmessages(d.text)
    porterr = a.Porterr()
    for i in porterr:
        objsanport = Sanport.objects.filter(san_id = id,index = i).first()
        if objsanport:
            data = {
                "enc_in": porterr[i]["enc_in"],
                "crc_err": porterr[i]["crc_err"],
                "crc_g_eof": porterr[i]["crc_g_eof"],
                "two_shrt": porterr[i]["two_shrt"],
                "two_long": porterr[i]["two_long"],
                "bad_eof": porterr[i]["bad_eof"],
                "enc_out": porterr[i]["enc_out"],
                "disc_c3": porterr[i]["disc_c3"],
                "link_fail": porterr[i]["link_fail"],
                "loss_sync": porterr[i]["loss_sync"],
                "loss_sig": porterr[i]["frjt"],
                "frjt": porterr[i]["loss_sig"],
                "fbsy": porterr[i]["fbsy"]
            }
            objclean = Sanlog_clean.objects.filter(San_log_id = d.id,San_portid_id = objsanport.id)
            if objclean:
                print("cuode")
            #     pass
            else:
                savelog = Sanlog_clean.objects.create(**data)
                savelog.San_log_id = d.id
                savelog.San_portid_id = objsanport.id
                savelog.log_time = d.dotime
                savelog.save()
from san.models import San
def bokestart():
    task = Task()
    task.start("博科地图数据清洗")
    sanobj = San.objects.filter(prodname="Brocade")
    for i in sanobj:
        try:
            Borde_huitu(i.id)
        except Exception as e:
            print(e)
    task.end()