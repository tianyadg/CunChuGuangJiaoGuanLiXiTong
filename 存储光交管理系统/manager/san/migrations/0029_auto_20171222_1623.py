# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-12-22 08:23
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('san', '0028_san_jifangadress'),
    ]

    operations = [
        migrations.AddField(
            model_name='host',
            name='jifangdress',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='san.Jifang', verbose_name='机房地点'),
        ),
        migrations.AlterField(
            model_name='host',
            name='position',
            field=models.CharField(blank=True, max_length=100, verbose_name='设备位置'),
        ),
    ]
