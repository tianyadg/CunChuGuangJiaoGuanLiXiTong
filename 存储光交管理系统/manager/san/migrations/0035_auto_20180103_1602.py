# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-01-03 08:02
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('san', '0034_auto_20180103_1552'),
    ]

    operations = [
        migrations.AlterField(
            model_name='storage',
            name='ST_usecap',
            field=models.CharField(blank=True, max_length=20, verbose_name='已使用容量'),
        ),
    ]
