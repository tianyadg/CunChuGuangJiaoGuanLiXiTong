# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-02-05 03:11
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('san', '0039_sanlog_clean'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Sanlog_clean',
        ),
    ]
