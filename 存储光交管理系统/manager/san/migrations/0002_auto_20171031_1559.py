# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-31 07:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('san', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sanport',
            name='index',
            field=models.CharField(blank=True, max_length=4, null=True, verbose_name='端口'),
        ),
        migrations.AlterField(
            model_name='sanport',
            name='port',
            field=models.CharField(blank=True, max_length=4, null=True, verbose_name='槽位端口'),
        ),
        migrations.AlterField(
            model_name='sanport',
            name='slot',
            field=models.CharField(blank=True, max_length=4, null=True, verbose_name='槽位'),
        ),
    ]
