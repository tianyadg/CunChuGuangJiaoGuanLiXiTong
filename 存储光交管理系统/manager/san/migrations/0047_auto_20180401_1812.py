# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-04-01 10:12
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('san', '0046_auto_20180215_1058'),
    ]

    operations = [
        migrations.AlterField(
            model_name='host',
            name='hostname',
            field=models.CharField(blank=True, max_length=30, verbose_name='所属部门'),
        ),
    ]
