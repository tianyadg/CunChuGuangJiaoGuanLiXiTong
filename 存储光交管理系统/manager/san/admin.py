# -*- coding: utf-8 -*-
from django.contrib import admin
from san import models
# Register your models here.
class BInline(admin.StackedInline):
    model = models.Wwn
@admin.register(models.Host)
class Hostadmin(admin.ModelAdmin):
    list_display = ("id","prodname","prodect","sn","ip","app","apptype","position")
    search_fields =("id","prodname","prodect","sn","ip","app","apptype","position")
    fields = (("prodname","prodect","sn"),("ip","app","apptype","position"),)
    inlines = [BInline, ]
@admin.register(models.Wwn)
class Wwnadmin(admin.ModelAdmin):
    list_display = ["id","wwn","host_wwn","Sanport_wwn","storage_wwn","wwnname"]
    search_fields = ["wwn",]
    # def HOST_IP(self,obj):
    #     a = models.Host.objects.filter(id = obj.host_wwn_id)
    #     print(a[0].ip)
    #     return u"%s"%a[0].ip
@admin.register(models.San)
class Sanadmin(admin.ModelAdmin):
    list_display = ("id","prodect","ip","sn","user","password","renote","domain","firmware","onlineport","availableport","position","jifangadress","starttime","endtime")
# @admin.register(models.Scriptlog)
# class Scriptlogadmin(admin.ModelAdmin):
#     list_display = ("id","time")
@admin.register(models.Errlog)
class erradmin(admin.ModelAdmin):
    list_display = ["id","elog","affirm","etime"]
@admin.register(models.Doment)
class Domadmin(admin.ModelAdmin):
    list_display = ("id","dotime","san","storage")
admin.site.site_header = "存储光交管理系统V1.0"
admin.site.site_title = '存储光交管理'

@admin.register(models.Sanport)
class Sanportadmin(admin.ModelAdmin):
    list_display = ("index","slot","port","speed","crc_err","enc_out","tx","rx","RX_Power","TX_Power","san")


@admin.register(models.Storage)
class Storageadmin(admin.ModelAdmin):
    list_display = ("id","ST_prodect","ST_ip","ST_sn","ST_user","ST_password","ST_renote","ST_totalrawcap","ST_usecap","ST_availcap","ST_totalcap","ST_firmware","ST_position","STjifang_adress","ST_starttime")
    list_editable = ["ST_totalrawcap","ST_usecap","ST_availcap","ST_totalcap","STjifang_adress"]

@admin.register(models.Jifang)
class Jifangadmin(admin.ModelAdmin):
    list_display =("id","dress")


@admin.register(models.Sanlog_clean)
class Sanlogcleanadmin(admin.ModelAdmin):
    list_display =("id","enc_in","crc_err","enc_out","link_fail","loss_sync","loss_sig","San_portid","San_log")
