__author__ = 'l4537'
#!/usr/bin/env python
#coding:utf-8
import time,os,re,sys
from scriptd.tasks import Task
def Huansuan(num):
    if "g" in num:
        c = num.strip("g")
        d = float(c) * 1024
        return round(d,3)
    elif "m" in num:
        return num.strip("m")
    elif "k" in num:
        c = num.strip("k")
        d = float(c) / 1024
        return round(d,3)
    elif num == "":
        return ""
    else:
        d = float(num) / 1024 / 1024
        return round(d,4)
def Re(res,total,id = "1"):
    #1.普通模式正则，2，多行模式正则
    if id == "1":
        data = re.findall(str(res),total)
    if id == "2":
        data = re.findall(str(res),total,re.S)
    return data

class CISCOMDS:
    def __init__(self,data):
        self.data = data
    def mds_port(self):
        pass
        data = Re("fc(\d+)\/(\d+)\s+(\S+)\s+FX\s+\S+\s+(\S+)",self.data,"2")
        #[('1', '1', '1', 'up'), ('1', '2', '1', 'up'), ('1', '3', '1', 'up'),
        return data
    def mds_wwn(self):
        pass
        data = Re("fc(\d+)\/(\d+)\s+(\S+)\s+\S{8}\s+(..:..:..:..:..:..:..:..)", self.data, "2")
        #[('1', '1', '1', '20:04:00:c0:dd:32:25:82'), ('1', '1', '1', '10:00:e0:97:96:11:b1:10'), ('
        return data
    def mds_portmess(self):
        #[(('1', '1', '13886304'（取消）, '22694144'（取消）, '0', '0', ' Wed Sep 16 12:49:31 2015'),
        data = Re("fc(\d+)\/(\d+) is up.*?(\S+)\s+discards,\s+(\S+).*?Interface last changed at(\s+\w+\s+\w+\s+\d+\s+\S+\s+\d+)*",self.data, "2")
        return data
    def mds_right(self):
        data = Re("fc(\d+)\/(\d+)\s+sfp\s+is\s+present.*?Tx Power\s+:\s+(\S+).*?Rx Power\s+:\s+(\S+)", self.data, "2")
        return data
    #（1，1，in,out）
    def mds_sulv(self):
        data = Re("fc(\d+)/(\d+)\s+(\d+)\s+\d+\s+(\d+)", self.data, "2")
        return data
#cisco光交
from san.ciscoscript import CISCOMDS
from san import models
def Cisco_to_data(id,text):
    data_shuju = CISCOMDS(text)
    mds_port = data_shuju.mds_port()
    mds_sulv = data_shuju.mds_sulv()
    #主端口更新
    task = Task()
    task.start(str("思科光交" + id + "-" + "开始刷新数据"))
    for i in mds_port:
        data ={
            "slot":int(i[0]),
            "port":int(i[1]),
            "speed":i[3],
            "type":i[2],
            "san_id":id,
        }
        obj = models.Sanport.objects.filter(slot=i[0], port=i[1], san_id=id).first()
        if not obj:
            obj = models.Sanport.objects.create(**data)
        else:
            obj.speed = i[3]
            obj.type = i[2]

        for s in mds_sulv:
            if i[0] == s[0] and i[1] == s[1]:
                obj.tx = s[3]
                obj.rx = s[2]
        mds_portmess = data_shuju.mds_portmess()
        for k in mds_portmess:
            if i[0] == k[0] and i[1] == k[1]:
                # obj.tx = Huansuan(k[3])
                # obj.rx = Huansuan(k[2])
                obj.enc_in = k[2]
                obj.crc_err = k[3]
                obj.Notes = k[4]

        mds_right = data_shuju.mds_right()
        for z in mds_right:
            if i[0] == z[0] and i[1] == z[1]:
                obj.RX_Power = z[2]
                obj.TX_Power = z[3]
        mds_wwn = data_shuju.mds_wwn()
        for w in mds_wwn:
            if i[0] == w[0] and i[1] == w[1]:
                wwn = models.Wwn.objects.update_or_create(wwn=w[3])
                wwn[0].Sanport_wwn = obj
                wwn[0].save()
        obj.save()
    objone = models.Sanport.objects.filter(san_id=id)
    onlineport = 0
    availableport = 0
    otherport =0
    for k in objone:
        if k.speed == "up":
            onlineport += 1
        elif k.speed == "down":
            availableport +=1
        elif k.speed == "notConnected":
            availableport += 1
        else:
            otherport +=1
    sanobj = models.San.objects.get(id = id)
    sanobj.availableport = availableport
    sanobj.onlineport = onlineport
    sanobj.otherport = otherport
    sanobj.save()
    task.end()


