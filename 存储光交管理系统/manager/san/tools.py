__author__ = 'l4537'
#!/usr/bin/env python
# -*- coding:utf-8 -*-
#coding:utf-8
#k,m,g换算为m
def Huansuan(num):
    if "g" in num:
        c = num.strip("g")
        d = float(c) * 1024
        return round(d,3)
    elif "m" in num:
        return num.strip("m")
    elif "k" in num:
        c = num.strip("k")
        d = float(c) / 1024
        return round(d,3)
    elif num == "":
        return ""
    else:
        d = float(num) / 1024 / 1024
        return round(d,4)

def Sum2(num):
    if "g" in num:
        c = num.strip("g")
        d = float(c) * 1000 * 1000 * 1000
        return d
    elif "m" in num:
        c = num.strip("m")
        d = float(c) * 1000 * 1000
        return d
    elif "k" in num:
        c = num.strip("k")
        d = float(c) * 1000
        return d
    elif num == "":
        return ""
    else:
        return num

