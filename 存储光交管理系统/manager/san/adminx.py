# -*- coding: utf-8 -*-
from san import models
import xadmin
# Register your models here.

#
# class BInline(admin.StackedInline):
#     model = models.Wwn
#
#
# @xadmin.register(models.Host)
class Hostadmin(object):
    list_display = ("id","prodname","prodect","sn","ip","app","apptype","position")
    search_fields =("id","prodname","prodect","sn","ip","app","apptype","position")
    list_filter = ["prodname","prodect","sn","ip","app","apptype","position"]
class Wwnadmin(object):
    list_display = ["id","wwn","host_wwn","Sanport_wwn","storage_wwn","wwnname"]
    search_fields = ["wwn"]
class Sanadmin(object):
    list_display = ("id","prodect","ip","sn","user","password","renote","domain","firmware","onlineport","availableport","position","jifangadress","starttime","endtime")
    list_fields = ["prodect","ip","renote","domain","jifangadress","starttime","endtime"]
class erradmin(object):
    list_display = ["id","elog","affirm","etime"]
class Domadmin(object):
    list_display = ("id","dotime","san","storage")
    list_filter = ["san","storage"]
class Sanportadmin(object):
    list_display = ("index","slot","port","speed","crc_err","enc_out","tx","rx","RX_Power","TX_Power","san")
class Storageadmin(object):
    list_display = ("id","ST_prodect","ST_ip","ST_sn","ST_user","ST_password","ST_renote","ST_totalrawcap","ST_usecap","ST_availcap","ST_totalcap","ST_firmware","ST_position","STjifang_adress","ST_starttime")
    list_editable = ["ST_totalrawcap","ST_usecap","ST_availcap","ST_totalcap","STjifang_adress","ST_user","ST_password"]
class Jifangadmin(object):
    list_display =("id","dress")
class Sanlogcleanadmin(object):
    list_display = ("id", "enc_in", "crc_err", "enc_out", "link_fail", "loss_sync", "loss_sig", "San_portid", "San_log")
    list_filter = ["log_time"]

xadmin.site.register(models.Sanlog_clean,Sanlogcleanadmin)
xadmin.site.register(models.Jifang,Jifangadmin)
xadmin.site.register(models.Storage,Storageadmin)
xadmin.site.register(models.Doment,Domadmin)
xadmin.site.register(models.Sanport,Sanportadmin)
xadmin.site.register(models.Errlog,erradmin)
xadmin.site.register(models.San,Sanadmin)
xadmin.site.register(models.Wwn,Wwnadmin)
xadmin.site.register(models.Host,Hostadmin)
