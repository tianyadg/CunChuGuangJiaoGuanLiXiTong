# -*- coding: utf-8 -*-
from django.db import models
# from storagedata.models import STHOST,STPORT
# Create your models here.
#主机

import django.utils.timezone as timezone
class Jifang(models.Model):
    dress = models.CharField(max_length=100,verbose_name="机房位置", null=True, blank=True)
    def __str__(self):
        name = self.dress
        return name
    class Meta:
        verbose_name = "机房地点"
        verbose_name_plural = verbose_name
class San(models.Model):
    STATUS_CHOICES =(
        ("T","telnet"),
        ("S","ssh"),
        ("C","console"),
        ("M","远程桌面")
    )
    prodname = models.CharField(max_length=20,verbose_name="厂商",blank=True)
    prodect = models.CharField(max_length=30,verbose_name="产品型号",blank=True)
    sn = models.CharField(max_length=40,verbose_name="设备SN",blank=True)
    ip = models.GenericIPAddressField(default="0.0.0.0",verbose_name="管理")
    user = models.CharField(max_length=100,verbose_name="用户名",blank=True)
    password = models.CharField(max_length=100,verbose_name="密码",blank=True)
    renote = models.CharField(max_length=10,verbose_name="远程方式",blank=True,choices=STATUS_CHOICES)
    hardware = models.CharField(max_length=100,verbose_name="硬件信息",blank=True)
    domain = models.CharField(max_length=100,verbose_name="domain",blank=True)
    firmware = models.CharField(max_length=100,verbose_name="固件",blank=True)
    position =models.CharField(max_length=100,verbose_name="设备位置",blank=True)
    jifangadress =models.ForeignKey(Jifang,verbose_name="机房位置",blank=True,null=True,on_delete=models.SET_NULL)
    starttime = models.DateTimeField(verbose_name="启用时间",blank=True,default = timezone.now)
    endtime = models.DateTimeField(verbose_name="过期时间",blank=True,default = timezone.now)
    availableport = models.CharField(max_length=10, verbose_name="可用端口", blank=True)
    nomodelport = models.CharField(max_length=10, verbose_name="无模块端口", blank=True)
    onlineport =  models.CharField(max_length=10, verbose_name="在线端口", blank=True)
    otherport = models.CharField(max_length=10, verbose_name="其他端口", blank=True)
    Notes = models.CharField(max_length=100, verbose_name="备注", blank=True)
    updatetime = models.DateTimeField(verbose_name="更新时间", blank=True,null=True)
    def __str__(self):
        return self.ip
    class Meta:
        verbose_name = "光交清单"
        verbose_name_plural = verbose_name
class Host(models.Model):
    prodname = models.CharField(max_length=20,verbose_name="厂商",blank=True)
    ip = models.GenericIPAddressField(default="0.0.0.0",verbose_name="IP地址")
    hostname = models.CharField(max_length=30,verbose_name="所属部门",blank=True)
    app = models.CharField(max_length=100,verbose_name="业务系统",blank=True)
    apptype = models.CharField(max_length=100,verbose_name="业务类型",blank=True)
    prodect = models.CharField(max_length=30,verbose_name="产品型号",blank=True)
    sn = models.CharField(max_length=40,verbose_name="产品sn",blank=True)
    position =models.CharField(max_length=100,verbose_name="设备位置",blank=True)
    jifangdress = models.ForeignKey(Jifang,verbose_name="机房地点",null=True,blank=True,on_delete=models.SET_NULL)
    def __str__(self):
        return self.ip
    class Meta:
        verbose_name = "主机信息表"
        verbose_name_plural = verbose_name
class Storage(models.Model):
    STATUS_CHOICES =(
        ("T","telnet"),
        ("S","ssh"),
        ("C","console"),
        ("M","远程桌面")
    )
    ST_prodname = models.CharField(max_length=20,verbose_name="厂商",blank=True)
    ST_prodect = models.CharField(max_length=30,verbose_name="产品型号",blank=True)
    ST_sn = models.CharField(max_length=40,verbose_name="设备SN",blank=True)
    ST_ip = models.GenericIPAddressField(default="0.0.0.0",verbose_name="管理")
    ST_user = models.CharField(max_length=100,verbose_name="用户名",blank=True)
    ST_password = models.CharField(max_length=100,verbose_name="密码",blank=True)
    ST_renote = models.CharField(max_length=10,verbose_name="远程方式",blank=True,choices=STATUS_CHOICES)
    ST_hardware = models.CharField(max_length=100,verbose_name="硬件信息",blank=True)
    ST_firmware = models.CharField(max_length=100,verbose_name="固件",blank=True)
    ST_position =models.CharField(max_length=100,verbose_name="设备位置",blank=True)
    STjifang_adress=models.ForeignKey(Jifang,verbose_name="机房位置",blank=True,null=True,on_delete=models.SET_NULL)
    ST_starttime = models.DateTimeField(verbose_name="启用时间",blank=True,null=True)
    ST_endtime = models.DateTimeField(verbose_name="过期时间",blank=True,null=True)
    ST_totalrawcap = models.CharField(max_length=10,verbose_name="裸容量(TB)",blank=True)
    ST_usecap = models.CharField(max_length=20,verbose_name="已使用容量(GB)",blank=True)
    ST_availcap = models.CharField(max_length=10,verbose_name="可用容量(GB)",blank=True)
    ST_totalcap = models.CharField(max_length=10,verbose_name="总容量(可用GB)",blank=True)
    ST_updatetime = models.DateTimeField(verbose_name="更新时间", blank=True, null=True)
    def __str__(self):
        name = self.ST_ip + "(" + self.ST_sn + ")"
        return name
    class Meta:
        verbose_name = "存储清单"
        verbose_name_plural = verbose_name
#光交
class Alias(models.Model):
    pass
class Zone(models.Model):
    pass
class Sanport(models.Model):
    index = models.IntegerField(blank=True,verbose_name="端口",null=True)
    slot = models.IntegerField(blank=True,verbose_name="槽位",null=True)
    port = models.IntegerField(blank=True,verbose_name="槽位端口",null=True)
    speed = models.CharField(max_length=20, blank=True,verbose_name="状态",null=True)
    type = models.CharField(max_length=10, blank=True,verbose_name="类型",null=True)
    fcid = models.CharField(max_length=10, blank=True,verbose_name="fcid",null=True)
    tx = models.CharField(max_length=10, blank=True,verbose_name="发送流量MB",null=True)
    rx = models.CharField(max_length=10, blank=True,verbose_name="接收流量MB",null=True)
    enc_in = models.CharField(max_length=10, blank=True,null=True)
    crc_err = models.CharField(max_length=10, blank=True,verbose_name="数据帧",null=True)
    crc_g_eof = models.CharField(max_length=10, blank=True,null=True)
    two_shrt = models.CharField(max_length=10, blank=True,null=True)
    two_long = models.CharField(max_length=10, blank=True,null=True)
    bad_eof = models.CharField(max_length=10, blank=True,null=True)
    enc_out = models.CharField(max_length=10, blank=True,verbose_name="帧外编码",null=True)
    disc_c3 = models.CharField(max_length=10, blank=True,null=True)
    link_fail = models.CharField(max_length=10, blank=True,null=True)
    loss_sync = models.CharField(max_length=10, blank=True,null=True)
    loss_sig = models.CharField(max_length=10, blank=True,null=True)
    frjt = models.CharField(max_length=10, blank=True,null=True)
    fbsy = models.CharField(max_length=10, blank=True,null=True)
    timeout_tx = models.CharField(max_length=20, blank=True,null=True)
    timeout_rx = models.CharField(max_length=10, blank=True,null=True)
    RX_Power = models.CharField(max_length=10,blank=True,verbose_name="接收光信号",null=True)
    TX_Power = models.CharField(max_length=10, blank=True,verbose_name="发送光信号",null=True)
    Notes = models.CharField(max_length=200, blank=True,verbose_name="备注",null=True)
    san= models.ForeignKey(San,verbose_name="光交",blank=True,null=True,on_delete=models.SET_NULL)
    def __str__(self):
        return "光交%s"%(self.index)
    class Meta:
        verbose_name = "光交端口信息表"
        verbose_name_plural = verbose_name

class Wwn(models.Model):
    from storagedata.models import STPORT,STHOST
    wwn = models.CharField(max_length=32,verbose_name="wwn",unique=True)
    host_wwn = models.ForeignKey(Host,verbose_name="主机",blank=True,null=True,on_delete=models.SET_NULL)
    Sanport_wwn = models.ForeignKey(Sanport,verbose_name="光交端口",blank=True,null=True,on_delete=models.SET_NULL)
    storage_wwn = models.ForeignKey(Storage,verbose_name="存储",blank=True,null=True,on_delete=models.SET_NULL)
    # storageport_wwn = models.OneToOneField(StoragePort, verbose_name="存储端口", blank=True, null=True, on_delete=models.SET_NULL)
    # hostgroup_wwn = models.ManyToManyField(Storagehost, verbose_name="存储host主机", blank=True)
    wwnname = models.CharField(max_length=128,verbose_name="wwn名称",blank=True,null=True)
    STport= models.OneToOneField(STPORT, verbose_name="SR存储端口", blank=True, null=True, on_delete=models.SET_NULL)
    SThost = models.ManyToManyField(STHOST, verbose_name="ST存储host主机", blank=True)
    def __str__(self):
        return self.wwn
    class Meta:
        verbose_name = "wwn信息表"
        verbose_name_plural = verbose_name
class Errlog(models.Model):
    STATUS_CHOICES =(
        ("Y",u"是"),
        ("N",u"否"),
    )
    elog = models.CharField(max_length=300,verbose_name="错误日志",blank=True)
    etime = models.DateTimeField(auto_now_add=True,verbose_name="报错时间",blank=True,null=True)
    affirm = models.CharField(max_length=10,verbose_name="是否已确认",choices=STATUS_CHOICES,blank=True)
    grade = models.IntegerField(verbose_name="告警等级",blank=True,default="4")
    def __str__(self):
        return "告警"
    class Meta:
        verbose_name = u"告警"
        verbose_name_plural = verbose_name
class Doment(models.Model):
    text = models.TextField(blank=True)
    storage = models.ForeignKey(Storage,verbose_name="存储信息",null=True,blank=True,on_delete=models.SET_NULL)
    san = models.ForeignKey(San,verbose_name="光交信息",null=True,blank=True,on_delete=models.SET_NULL)
    dotime = models.DateTimeField(verbose_name="添加时间",blank=True)
    class Meta:
        verbose_name = "日志"
        verbose_name_plural = verbose_name

class Sanlog_clean(models.Model):
    San_portid = models.ForeignKey(Sanport,verbose_name="端口清洗",null=True,blank=True,on_delete=models.SET_NULL)
    San_log = models.ForeignKey(Doment,verbose_name="log日志清洗",null=True,blank=True,on_delete=models.SET_NULL)
    San_wwn = models.CharField(max_length=32,verbose_name="wwn",null=True,blank=True)
    enc_in = models.CharField(max_length=10, blank=True,null=True)
    crc_err = models.CharField(max_length=10, blank=True,verbose_name="数据帧",null=True)
    crc_g_eof = models.CharField(max_length=10, blank=True,null=True)
    two_shrt = models.CharField(max_length=10, blank=True,null=True)
    two_long = models.CharField(max_length=10, blank=True,null=True)
    bad_eof = models.CharField(max_length=10, blank=True,null=True)
    enc_out = models.CharField(max_length=10, blank=True,verbose_name="帧外编码",null=True)
    disc_c3 = models.CharField(max_length=10, blank=True,null=True)
    link_fail = models.CharField(max_length=10, blank=True,null=True)
    loss_sync = models.CharField(max_length=10, blank=True,null=True)
    loss_sig = models.CharField(max_length=10, blank=True,null=True)
    frjt = models.CharField(max_length=10, blank=True,null=True)
    fbsy = models.CharField(max_length=10, blank=True,null=True)
    timeout_tx = models.CharField(max_length=20, blank=True,null=True)
    timeout_rx = models.CharField(max_length=10, blank=True,null=True)
    RX_Power = models.CharField(max_length=10,blank=True,verbose_name="接收光信号",null=True)
    TX_Power = models.CharField(max_length=10, blank=True,verbose_name="发送光信号",null=True)
    log_time =  models.DateTimeField(verbose_name="数据时间",blank=True,null=True)
    class Meta:
        verbose_name = "日志清洗"
        verbose_name_plural = verbose_name
