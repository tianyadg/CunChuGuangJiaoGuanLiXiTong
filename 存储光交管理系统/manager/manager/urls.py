"""manager URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from san import views
from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
# from django.conf.urls.static import static
from django.conf.urls import include
from ceshi.views import fenye
import work
# from script.upload import upload_image
from django.views.decorators.cache import cache_page
import xadmin
urlpatterns = [
    url(r'^admin/',xadmin.site.urls),
    url(r"^$",views.index,name = "index"),
    url(r"index",views.index,name = "index"),
    url(r"^log_mess/(?P<log_id>[0-9]+)/",views.log_xiangxi,name = "log_mess"),
    url(r"update/(?P<update_id>[0-9]+)/",views.logupdate,name = "logupdate"),
    url(r"sanlink/$",views.Sanlink,name="sanlink"),
    url(r"storage/",views.Storges,name="storge"),
    url(r"caiji/",views.Caiji,name="Caiji"),
    url(r"yewu/",cache_page(60 * 90)(views.Yewu),name="yewu"),
    url(r"alert/",views.Alert,name="alert"),
    url(r"ceshi/",include('ceshi.urls')),
    url(r"fenye/",fenye),
    url(r"^sanupdateall/",views.Sanupdateall,name="sanupdateall"),
    url(r"^san_huitu_update/",views.San_huitu_update,name="san_huitu_update"),
    url(r"^san_liuliang_im/",views.san_liuliang_im,name="san_liuliang_im"),
    url(r"^caiji_san_port/",views.Caiji_san_port,name="caiji_san_port"),
    url(r"^san_err_get_shuju/",views.San_err_get_shuju,name="san_err_get_shuju"),
    url(r"^host_search_ajax/",views.host_search_ajax,name="host_search_ajax"),
    url(r"^san_sel_update_ajax/",views.San_sel_update_ajax,name="san_sel_update_ajax"),
    url(r"sanlink/sanmess/(?P<sanmess_id>[0-9]*)",cache_page(60 * 15)(views.Sanmess),name="sanmess"),
    url(r"sancompall/$",views.Sancompall,name="sancompall"),
    url(r"^host/$",views.Host,name="host"),
    url(r"^Addhost/$", views.Addhost, name="addhost"),
    url(r"^addhost_ajax/$",views.addhost_ajax,name="addhost_ajax"),
    url(r"storagemess/(?P<storage_id>[0-9]*)",cache_page(60 * 15)(views.Storagemess),name="storagemess"),
    url(r"^storage_log/",views.Storages_log,name= "storages_log"),
    url(r"san_log/",views.San_log,name= "san_log"),
    url(r"san_port_tables/",views.San_port_tables,name= "san_port_tables"),
    url(r"san_port_err_tables/",views.San_port_err_tables,name= "san_port_err_tables"),
    url(r"san_liuliang_import/",views.san_liuliang_import,name= "san_liuliang_import"),
    url(r"^logout/$",views.Logout,name= "logout"),
    url(r"^login/$",views.Auto_login,name= "auto_login"),
    url(r"login_ajax/",views.Login_ajax,name= "login_ajax"),
    url(r"search_messages/",views.Search_messages,name= "search_messages"),
    url("^work",include('work.urls')),
    url("^storages",include('storagedata.urls')),
    # url(r'^admin/uploads/(?P<dir_name>[^/]+)$', upload_image, name='upload_image'),
    url(r"^hot_wwn/",cache_page(60 * 150)(views.Hot_wwn),name= "Hot_wwn"),
    # url(r"^uploads/(?P<path>.*)$", views.static.serve, {"document_root": settings.MEDIA_ROOT, }),
]

# static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)