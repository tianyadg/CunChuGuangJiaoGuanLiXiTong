# coding:utf-8
#消费者生产者模型
import gevent
import gevent.monkey
gevent.monkey.patch_all()
import requests
from gevent.queue import Queue, Full, Empty
from gevent.pool import Pool


# if Queue() have no parameter It's unlimited
# out jd_queue just put in 100 msg.......
msg_queue = Queue(100)  #最多100个
jd_pool = Pool(10)   #（最多同时进行10个）
jd_msg = "包子"
test_url = "http://www.xiachufang.com"

def deal_with():
    while True:
        try:
            now_id = gevent.getcurrent()
            msg = msg_queue.get_nowait()  #获取一个数据，并且获取到啦
            print ("吃啦一个包子 " + msg)
            print ('查看目前获取的东西 %s',now_id)
            requests.get(test_url)
            print ('获取到啦地址', now_id)
        except Empty:
            gevent.sleep(0)

#生产者
def product_msg(jd_msg):
    while True:
        try:
            msg_queue.put_nowait(jd_msg)  #（生产啦一个保证）
            print (msg_queue.qsize(),"查看包子裤的信息")
        except Full:
            gevent.sleep(5)


jd_pool.add(gevent.spawn(product_msg, jd_msg))#添加生产者
for i in range(10):
    jd_pool.add(gevent.spawn(deal_with))#添加10个消费者
jd_pool.join()