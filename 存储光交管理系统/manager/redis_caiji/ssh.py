#!/usr/bin/envpython
#-*-coding:utf-8-*-
import redis
import json
from pexpect import pxssh
import pexpect
import re
import time
import threading
def Re(res,total,id = "1"):
    if id == "1":
        data = re.findall(str(res),total)
    if id == "2":
        data = re.findall(str(res),total,re.S)
    return data
#执行命令
class linuxforssh:
    def __init__(self,ip,user,password,port):
        self.user = user
        self.ip = ip
        self.password = password
        self.port = port
        self.s = pxssh.pxssh()
    def autolink(self):
        try:
            self.s.login(self.ip, self.user, self.password,port=self.port)
            return True
        except Exception as e:
            print("连接失败%s"%(e))
        return False
    def sendcommend(self,commend):
        self.s.sendline(commend)
        if self.s.prompt():
            return bytes.decode(self.s.before)
        else:
            return "命令失败"
    def logout(self):
        self.s.logout()

a = linuxforssh("192.168.20.210","root","root",22)
a.autolink()
# b = a.sendcommend("cat /proc/net/dev")
# k = Re("(\S+):\s+(\d+)\s+\d+\s+\d+\s+\d+\s+\d+\s+\d+\s+\d+\s+\d+\s+(\d+)",b)
# eth = {}
# for i in k:
#     if i[0] == "lo":
#         pass
#     else:
#         eth[i[0]] = [int(int(i[1])/1024),int(int(i[2])/1024)]
# neteth = json.dumps(eth)
# print(type(neteth),neteth)


diskio = {}
b = a.sendcommend("iostat -x -m -d")
k = Re("(\S+)\s+\d+.\d+\s+\d+.\d+\s+\d+.\d+\s+\d+.\d+\s+(\d+.\d+)\s+(\d+.\d+)\s+\d+.\d+\s+\d+.\d+\s+\d+.\d+\s+(\d+.\d+)\s+(\d+.\d+)\s+(\d+.\d+)\s+(\d+.\d+)",b)
for i in k:
    diskio[i[0]] = {}
    diskio[i[0]]["rx"] = i[1]
    diskio[i[0]]["tx"] = i[2]
    diskio[i[0]]["r_wait"] = i[3]
    diskio[i[0]]["w_wait"] = i[4]
    diskio[i[0]]["svctm"] = i[5]
    diskio[i[0]]["util"] = i[6]
