#!/usr/bin/envpython
#-*-coding:utf-8-*-
import redis

class RedisHelper(object):
    def __init__(self):
        self.__conn = redis.Redis(host='127.0.0.1',port=6379)#连接Redis
        self.channel = "redisChat" #定义名称

    def publish(self,msg):#定义发布方法
        self.__conn.publish(self.channel,msg)
        return True
    def subscribe(self):#定义订阅方法
        pub = self.__conn.pubsub()
        pub.subscribe(self.channel)
        pub.parse_response()
        return pub
#接收订阅
def pool_jieshou():
    obj = RedisHelper()
    redis_sub = obj.subscribe()
    while True:
        msg= redis_sub.parse_response()
        print(msg[2])
#list及strings采用这个登入
def loginredis():
    pool = redis.ConnectionPool(host='127.0.0.1', port=6379)
    r = redis.Redis(connection_pool=pool)
    return r
"""
#!/usr/bin/env python
# -*- coding:utf-8 -*-
#发布
from RedisHelper import RedisHelper

obj = RedisHelper()
obj.publish('hello')#发布
"""