#!/usr/bin/envpython
#-*-coding:utf-8-*-
from gevent import monkey; monkey.patch_all()
import gevent
from gevent.queue import Queue
from gevent.pool import Group
from gevent import pool
def f(a):
    gevent.sleep(2)
    print(a)

pool = pool.Pool(3)
pool.spawn(f,3)
pool.spawn(f,5)
pool.spawn(f,6)
print(pool)
print("a")
