#!/usr/bin/envpython
#-*-coding:utf-8-*-
import re
from san import models
def Re(res,total,id = "1"):
    if id == "1":
        data = re.findall(str(res),total)
    if id == "2":
        data = re.findall(str(res),total,re.S)
    return data
from scriptd.fsjinfo import fsjinfo
#存储id跟日志
def Fsttodata(id,log):
    id = id.id
    dome = fsjinfo(log)
    # 聚合信息
    juhezu = dome.juhemess()
    obj = models.Hostgroup.objects.filter(storage_id=id)
    for i in juhezu:
        objnone = models.Hostgroup.objects.filter(hostgroupname = str(i[1]),portgroupname = str(i[0]),lungroupname =str(i[2]),storage_id = id)
        if not objnone:
            data = {
                "name": str(""),
                "hostgroupname": str(i[1]),
                "portgroupname": str(i[0]),
                "lungroupname": str(i[2]),
                "storage_id": id,
            }
            try:
                models.Hostgroup.objects.create(**data)
            except Exception as e:
                print(i,e)
            # break
    # 端口组信息
    port_group = dome.portgroup()
    # 添加wwn 及对应信息
    from scriptd.towwn import WWn
    for i in port_group[0]:
        if not models.StoragePort.objects.filter(name = str(i),storage_id=id):
            data = {
                "name": str(i),
                "storage_id": id,
            }
            try:
                obj = models.StoragePort.objects.create(**data)
            except Exception as e:
                print(i,e)
            wwn = WWn(port_group[0][i])
            try:
                objwwn = models.Wwn.objects.update_or_create(wwn=wwn)
                objwwn[0].storage_wwn_id = id
                objwwn[0].storageport_wwn_id = obj.id
                objwwn[0].wwnname = i
                objwwn[0].save()
            except Exception as e:
                print(e)
            for p in port_group[1]:
                if i in port_group[1][p]:
                    a = models.Hostgroup.objects.filter(portgroupname=p, storage_id=id).first()
                    if a:
                        try:
                            obj.portgroupname.add(a)
                        except Exception as e:
                            print(a,e)
    # 添加主机及主机组
    hostgroup = dome.Hostgroup()
    for i in hostgroup[0]:
        if not  models.Storagehost.objects.filter(hostname=i[0],storage_id=id,hosttype=i[2]):
            data = {
                "hostname": i[0],
                "hosttype": i[2],
                "storage_id": id,
            }
            a = models.Storagehost.objects.create(**data)
            wwn = WWn(i[1])
            objhostwwn = models.Wwn.objects.update_or_create(wwn=wwn)
            objhostwwn[0].storage_wwn_id = id
            # objhostwwn[0].hostgroup_wwn_id = a.id
            objhostwwn[0].save()
            objhostwwn[0].hostgroup_wwn.add(a)
            for h in hostgroup[1]:
                # print(hostgroup[1][h])
                if i[0] in hostgroup[1][h]:
                    try:
                        a.hostgroup.add(models.Hostgroup.objects.filter(hostgroupname=h, storage_id=id).first())
                    except Exception as e:
                        print(e,"加入失败")
