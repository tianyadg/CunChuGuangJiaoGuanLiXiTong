#coding:utf-8
import re,json
def Re(res,total,id = "1"):
    if id == "1":
        data = re.findall(str(res),total)
    if id == "2":
        data = re.findall(str(res),total,re.S)
    return data
class Hitachi():
    def __init__(self,data):
        self.data = data

    def portdata(self):
        data = Re(r"(\d\w),\w+,\w+,\d+,\w+,Point to Point,\w+,(\S{16}),(\S+),",self.data,"2")
        return data
    def host(self):
        data = Re(r"(\d\w),(\S+),\d+,,(\w{16}),,\d+,(\w+-\w+)",self.data,"2")
        return data
    def lundata(self):
        data = Re("\d\w,(\S+),\d+,,\w+,(..:..:..)",self.data,"2")
        return data
from scriptd.towwn import WWn
from san import models
class Huawei:
    def __init__(self,data):
        self.data = data
    def portdata(self):
        portdata = Re(r"ID:\s+(CTE\d.\w\d.IOM\d.P\d).*?WWN:\s+(\S{16})",self.data,"2")
        pdata = []
        for i in portdata:

            if i[1][0:2] == "50" or i[1][0:2] == "0x":
                pass
            else:
                pdata.append(i)
        return pdata
    def host(self):
        #ID ,mingchen,wwn
        hostdata = Re(r"Id:\s+(\d+)\s+Name:\s*(\S+).*?((Type:\s*FC\s+WWN:\s*(\S+)\s+MultiPath Type:\s+\w+\s+)+)",self.data,"2")
        dhost = {}
        for i in hostdata:
            dhost[i[0]] = {}
            wwn = Re(r"WWN:\s+0x(\S{16})",i[2])
            dhost[i[0]]["name"] = i[1]
            dhost[i[0]]["wwn"] = wwn
            # print(i[2])
        #ID,name + hostid
        hostgroupdata = Re(r"Id:\s+(\d+)\s+Name:\s+(\S+).*?Host Nums:\s*\d+\s+Host List:\s+((ID:\d+\s+\S+\s+)*)",self.data,"2")
        hostgroup = {}
        for i in hostgroupdata:
            hostgroup[i[0]] ={}
            num = list(set(Re("ID:\s*(\d+)",i[2])))
            hostgroup[i[0]]["name"] =i[1]
            hostgroup[i[0]]["HOSTID"] =num
        return [hostgroup,dhost]
    def lundata(self):
        #lun组id，lun组名称，lun组所存在的lun
        lungroupdata = Re("Id:\s+(\d+)\s+Name:\s+(\S+).*?LUN INFO:\s+((LUN ID:\s+\d+\s+)*)Mapping View INFO",self.data,"2")
        datalungroup = {}
        for i in lungroupdata:
            datalungroup[i[0]] = {}
            datalungroup[i[0]]["name"] = i[1]
            datalungroup[i[0]]["LUNID"] =Re(r"LUN ID:\s+(\d+)",i[2])
        #lunid,lun名称，lun类型，lun大小
        lundata = Re(r"ID:\s+(\d+)\s+Name:\s+(\S+).*?Type:\s+(\S+)\s+Capacity:\s+(\d+)",self.data,"2")
        #计算  反馈   LUN组ID：LUN组名称:df:LU信息:{LUNID}
        luniddata = {}
        for i in lundata:
            luniddata[i[0]] = {}
            luniddata[i[0]]["name"] = i[1]
            luniddata[i[0]]["type"] = i[2]
            luniddata[i[0]]["size"] = i[3]
        return [datalungroup,luniddata]
    #主机组-端口组-lun组
    def maping(self):
        data = Re("Id:\s(\d+)\s+Name:\s(\S+)\s+.*?Map Attribute:\w+\s+Map Type:\S+\s+Host Group Id:(\d+)\s+Port Group Id:(\d+)\s+Lun Group Id:(\d+)", self.data, "2")
        # data = Re("Id:\s(\d+)\s+Name:\s(\S+)\s+Map Attribute:\w+\s+Map Type:\S+\s+Host Group Id:(\d+)\s+Port Group Id:(\d+)\s+Lun Group Id:(\d+)", self.data, "2")
        return data
    def disk(self):
        data = Re("(DAE\S+)\s+(\S+)\s+(\S+)\s+\S+\s+(\S+)\s+Member\s+Disk\s+(\d+)",self.data, "2")
        return data