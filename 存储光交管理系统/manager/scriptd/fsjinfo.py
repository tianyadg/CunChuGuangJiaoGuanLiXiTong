#coding:utf-8
import re,json
def Re(res,total,id = "1"):
    if id == "1":
        data = re.findall(str(res),total)
    if id == "2":
        data = re.findall(str(res),total,re.S)
    return data
class fsjinfo():
    def __init__(self,data,numstid = "1"):
        self.data = re.sub("-{3,}","",data)
        self.numstid = numstid
    def raidinfo(self):
        #('0', 'Raid5-00', 'RAID5', '1677312', '37887')
        redata = Re("(\d+)\s+(\S+)\s+(\S+)\s.*?Available\s+(\d+)\s+(\d+)", self.data)
        return redata
    def diskinfo(self):
        # ('DE#00-Disk#0', 'Present', '600GB', '2.5', 'Online', '10000', 'Data')
        redata = Re("(DE\S+)\s+(\w+)\s+(\S+)\s+(\d.\d)\s+(\w+)\s+(\d+)\s+(\w+)", self.data)
        return redata
#得到所有的主机信息，和主机组对应的端口组
    def Hostgroup(self):
        redata = Re("\d+\s+(\S+)\s+(\S{16})\s+\d+\s+(\w+)",self.data)
        # print(redata)
        if self.numstid == "2":
            totalhostgroup = Re(r"Volumes\s+\d+\s+\S+\s+\d+\s+(\S+)\s+\d+\s+\S+.*?Name\s+((CE#\d CM#\d CA#\d Port#\d\s+\d+\s+\S+\s+)*)",self.data, "2")
        elif self.numstid == "1":
            totalhostgroup = Re(r"Volumes\s+\d+\s+\S+\s+\d+\s+(\S+)\s+\d+\s+\S+.*?Name\s+((CM#\d CA#\d Port#\d\s+\d+\s+\S+\s+)*)",self.data, "2")
        # print(totalhostgroup)
        total_hostgroup ={}
        for i in totalhostgroup:
            # total_group[i] =[]
            if self.numstid == "2":
                mess = Re(r"CE#\d CM#\d CA#\d Port#\d\s+\d+\s+(\S+)",i[1])
            elif self.numstid == "1":
                mess = Re(r"CM#\d CA#\d Port#\d\s+\d+\s+(\S+)",i[1])
            mess = list(set(mess))
            total_hostgroup[i[0]] = mess
        host_group = [redata,total_hostgroup]
        return host_group
#得到所有的端口信息，和端口组对应端口
    def portgroup(self):
        if self.numstid == "1":
            total = Re(r"(CM#\d CA#\d Port#\d).*?Port WWN\s+\[(\S+)\]",self.data,"2")
            totalport = {}
            for i in total:
                totalport[i[0]] = i[1]
            totalgroup =  Re(r"Volumes\s+\d+\s+(\S+)\s+\d+\s+\S+\s+\d+\s+\S+.*?Name\s+((CM#\d CA#\d Port#\d\s+\d+\s+\S+\s+)*)",self.data,"2")
            total_group = {}
        elif self.numstid == "2":
            total = Re(r"(CE#\d CM#\d CA#\d Port#\d).*?Port WWN\s+\[(\S+)\]",self.data,"2")
            totalport = {}
            for i in total:
                totalport[i[0]] = i[1]
            totalgroup =  Re(r"Volumes\s+\d+\s+(\S+)\s+\d+\s+\S+\s+\d+\s+\S+.*?Name\s+((CE#\d CM#\d CA#\d Port#\d\s+\d+\s+\S+\s+)*)",self.data,"2")
            total_group = {}
        for i in totalgroup:
            # total_group[i] =[]
            if self.numstid == "1":
                mess = Re(r"CM#\d CA#\d Port#\d",i[1])
            elif self.numstid == "2":
                mess = Re(r"CE#\d CM#\d CA#\d Port#\d", i[1])
            mess = list(set(mess))
            total_group[i[0]] = mess
        portmess = [totalport,total_group]
        return portmess
#得到端口组-主机组-lun组  一一对应
    def juhemess(self):
        mess = Re(r"Volumes\s+(\d+)\s+(\S+)\s+(\d+)\s+(\S+)\s+(\d+)\s+(\S+)\s+No",self.data, "2")
        return mess
    #获取lun信息及lun组
    def lun(self):
        #lunid，lun名称，raid-id,raid名称('0', 'VMvolume01', '0', 'HGT-Raid5-01', '819200')
        lundata= Re(r"(\d+)\s+(\S+)\s+Available\s+Standard\s+(-)*\s+(\d+)\s+(\S+)\s+(\d+)", self.data, "2") #('0', 'VMvolume01', '0', 'HGT-Raid5-01', '819200')
        # lundata = Re(r"(\d+)\s+(\S+)\s+Available\s+Standard\s+-\s+(\d+)\s+(\S+)\s+(\d+)", self.data, "2") #('0', 'VMvolume01', '0', 'HGT-Raid5-01', '819200')
        lungroup = Re(r"Name\s+(\d+)\s+(\S+)\s+\w+\s+\S+\s+<Mapping>.*?No.  Name\s+((\d+\s+\d+\s+\S+\s+-\s+)+)", self.data,"2")
        #('0', 'VMvolume01', '0    0 VMLun01          -\r\n\r\n     1    1 VMLun02          -\r\n\r\n\r\n\r\n', '1    1 VMLun02          -\r\n\r\n\r\n\r\n')
        #{lunid{"name"："zi"}}
        #获取lun组{'3': {'LUNID': ['10', '11', '12', '13', '101', '102', '116', '117', '118', '126', '127'], 'name': 'CirtixLun'}
        # print(lungroup)
        lungroupname ={}
        for i in lungroup:
            lungr = list(set(Re(r"\d+\s+(\d+)\s+(\S+)",i[2],"2")))
            for k in lungr:
                lungroupname[k[0]] =  {}
                lungroupname[k[0]]["name"] = k[1]
        for i in lungroupname:
            listlun = []
            for k in lungroup:
                lungr = list(set(Re(r"\d+\s+(\d+)\s+(\S+)", k[2], "2")))
                for z in lungr:
                    if i == z[0]:
                        listlun.append(k[0])
            lungroupname[i]["LUNID"] = listlun
        return [lundata,lungroupname]
