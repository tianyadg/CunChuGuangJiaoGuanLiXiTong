#coding:utf-8
import os,time
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
class Log:
    def __init__(self):
        self.lujing = os.path.join(BASE_DIR, "logger" + os.path.sep +"app.log")
        self.append = open(self.lujing,"a")
    def info(self,text):
        data = "[" + str(time.strftime('%Y-%m-%d-%H-%m-%S')) + "]----" +" [info]" + "----<"+ str(text) + ">" + "\n"
        self.append.write(data)
    def warning(self,text):
        data = "[" + str(time.strftime('%Y-%m-%d-%H-%m-%S')) + "]----" +" [warning]" + "----<"+ str(text) + ">" + "\n"
        self.append.write(data)
    def error(self,text):
        data = "[" + str(time.strftime('%Y-%m-%d-%H-%m-%S')) + "]----" +" [error]" + "----<"+ str(text) + ">" + "\n"
        self.append.write(data)
    def critical(self,text):
        data = "[" + str(time.strftime('%Y-%m-%d-%H-%m-%S')) + "]----" +" [critical]" + "----<"+ str(text) + ">" + "\n"
        self.append.write(data)
    def debug(self,text):
        data = "[" + str(time.strftime('%Y-%m-%d-%H-%m-%S')) + "]----" +" [debug]" + "----<"+ str(text) + ">" + "\n"
        self.append.write(data)
